#include "widget.h"
#include "ui_widget.h"
#include <QtSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QDebug>

widget::widget( QWidget *parent ) : QWidget (parent) , ui (new Ui::widget)
{
    ui->setupUi(this);

    connect ( ui->bPrende , SIGNAL(clicked()) , this , SLOT(on_encenderPushButtom_clicked()));
    connect ( ui->bApaga , SIGNAL(clicked()) , this , SLOT(on_apagarPushButtom_clicked()) );


    arduino_is_avalible = false ;
    arduino_port_name = "";
    arduino = new QSerialPort ;

    qDebug () <<"Number of available ports: " <<QSerialPortInfo::availablePorts().length();
    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()) {
        if ( serialPortInfo.hasVendorIdentifier() ){
            qDebug() << "Vendor ID: " <<serialPortInfo.vendorIdentifier();
        }
        qDebug() <<"Has product ID:" <<serialPortInfo.hasProductIdentifier();
        if (serialPortInfo.hasProductIdentifier()){
            qDebug() <<"Prduct ID:" <<serialPortInfo.productIdentifier();
        }
    }

    foreach ( const QSerialPortInfo &serialPortInfo , QSerialPortInfo::availablePorts() ){
        if (serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier()){
            if( serialPortInfo.vendorIdentifier() == arduino_nano_vendor_id ){
                if ( serialPortInfo.productIdentifier() == arduino_nano_product_id ) {
                    arduino_port_name = serialPortInfo.portName ();
                    arduino_is_avalible = true;
                }
            }
        }
    }
    if (arduino_is_avalible){
        arduino->setPortName(arduino_port_name);
        arduino->open(QSerialPort::WriteOnly);
        arduino->setBaudRate(QSerialPort::Baud9600);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        arduino->setFlowControl(QSerialPort::NoFlowControl);
    }else
        QMessageBox::warning(this,"Port error","No se pudo encontrar el Ardu");
}

widget::~widget()
{
    delete ui;
}

void widget::on_encenderPushButtom_clicked()
{
    if ( arduino->isWritable() ){
        arduino->write ("255");
    }//else{
        //qDebug <<"No se pudo escribir el serial";
    //}
}

void widget::on_apagarPushButtom_clicked(){
    if ( arduino->isWritable() ){
        arduino->write ("0");
    }//else{
        //qDebug <<"No se pudo escribir el serial";
    //}
}

