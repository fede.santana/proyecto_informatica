#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class widget;
}

class QSerialPort;

class widget : public  QWidget
{
    Q_OBJECT

public:
    explicit widget ( QWidget *parent = 0 ) ;
    ~widget();
private slots:
    void on_encenderPushButtom_clicked();
    void on_apagarPushButtom_clicked();

private:
    Ui::widget *ui ;
    QSerialPort *arduino ;
    static const quint16 arduino_nano_vendor_id = 0x1a86 ; //para completar estos, ejecutar por linea de comando dmesg, una vez que pueda conectar el arduino
    static const quint16 arduino_nano_product_id = 0x7523 ;
    QString arduino_port_name ;
    bool arduino_is_avalible ;
};

#endif // WIDGET_H
