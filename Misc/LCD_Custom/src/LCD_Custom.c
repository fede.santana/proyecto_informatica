/*
===============================================================================
 Name        : pruebaBiblioteca2.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <DR_Inicializacion.h>
#include <PR_Buzzer.h>
#include <PR_lcd.h>
#include <DR_lcd.h>
#define LCD_CONTROL 1
#define LCD_DATA 0
// TODO: insert other include files here

// TODO: insert other definitions and declarations here




int main(void) {

    // TODO: insert code here
	Inicializacion();
    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    Buzzer(1);
    LCD_Push( 0 + ( 0x80 | LINE_1_ADDRESS ), LCD_CONTROL );
    LCD_Push(0x00,LCD_DATA);
    LCD_Push( 1 + ( 0x80 | LINE_1_ADDRESS ), LCD_CONTROL );
	LCD_Push(0x01,LCD_DATA);
    LCD_Push( 0 + ( 0x80 | LINE_2_ADDRESS ), LCD_CONTROL );
	LCD_Push(0x02,LCD_DATA);
    LCD_Push( 1 + ( 0x80 | LINE_2_ADDRESS ), LCD_CONTROL );
	LCD_Push(0x03,LCD_DATA);
	LCD_Display("UTN - INFO 2",0,3);
	LCD_Display("The Hawkeye",1,3);
    while(1) {

    }
    return 0 ;
}
