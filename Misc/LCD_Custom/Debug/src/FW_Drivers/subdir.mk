################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/FW_Drivers/DR_Buzzer.c \
../src/FW_Drivers/DR_Inicializacion.c \
../src/FW_Drivers/DR_PLL.c \
../src/FW_Drivers/DR_RGB.c \
../src/FW_Drivers/DR_SysTick.c \
../src/FW_Drivers/DR_gpio.c \
../src/FW_Drivers/DR_lcd.c \
../src/FW_Drivers/DR_pinsel.c \
../src/FW_Drivers/PR_Buzzer.c \
../src/FW_Drivers/PR_RGB.c \
../src/FW_Drivers/PR_lcd.c 

OBJS += \
./src/FW_Drivers/DR_Buzzer.o \
./src/FW_Drivers/DR_Inicializacion.o \
./src/FW_Drivers/DR_PLL.o \
./src/FW_Drivers/DR_RGB.o \
./src/FW_Drivers/DR_SysTick.o \
./src/FW_Drivers/DR_gpio.o \
./src/FW_Drivers/DR_lcd.o \
./src/FW_Drivers/DR_pinsel.o \
./src/FW_Drivers/PR_Buzzer.o \
./src/FW_Drivers/PR_RGB.o \
./src/FW_Drivers/PR_lcd.o 

C_DEPS += \
./src/FW_Drivers/DR_Buzzer.d \
./src/FW_Drivers/DR_Inicializacion.d \
./src/FW_Drivers/DR_PLL.d \
./src/FW_Drivers/DR_RGB.d \
./src/FW_Drivers/DR_SysTick.d \
./src/FW_Drivers/DR_gpio.d \
./src/FW_Drivers/DR_lcd.d \
./src/FW_Drivers/DR_pinsel.d \
./src/FW_Drivers/PR_Buzzer.d \
./src/FW_Drivers/PR_RGB.d \
./src/FW_Drivers/PR_lcd.d 


# Each subdirectory must supply rules for building sources it contributes
src/FW_Drivers/%.o: ../src/FW_Drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -D__REDLIB__ -I"/home/fg/Documents/MCUXpresso_11.0.0_2516/workspace/LCD_Custom/inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


