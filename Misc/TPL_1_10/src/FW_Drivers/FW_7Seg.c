/**
 	\file KitInfo2FW_7Seg.c
 	\brief Drivers del display de 7 segmentos
 	\details Expansion 2
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/

#include "Aplicacion.h"
extern uint8_t Tabla_Digitos_BCD_7seg[];
extern volatile uint8_t msgDisplay[];		//!< Buffer de display
void BarridoDisplay( void );
void display (uint8_t numero, uint8_t digito);
uint8_t decode (uint8_t number);
/**
	\fn void BarridoDisplay( void )
	\brief Barrido del display de 7 segmentos
 	\param void
	\return void
*/
/*
uint8_t decode (uint8_t number)
{
	switch (number)
	{
		case 0:
			return 0x01;
			case 1:
			return 0x4f;
		case 2:
			return 0x12;
		case 3:
			return 0x06;
		case 4:
			return 0x4c;
		case 5:
			return 0x24;
		case 6: 
			return 0x60;
		case 7:
			return 0x0f;
		case 8:
			return 0x00;
		case 9:
			return 0x0c;
		default:
			return 0xff;
	}
}
*/
void my_Display (uint32_t number)
{
	int digito=0;
    while(number%10){
	
	msgDisplay[digito]	= Tabla_Digitos_BCD_7seg[(number % 10)];
	number /= 10;
	digito++;
	}

}


void BarridoDisplay( void )
{
	static int i=0;
	SetPIN(EXPANSION0,0);
	SetPIN(EXPANSION1,0);
	SetPIN(EXPANSION2,0);
	SetPIN(EXPANSION3,0);
	SetPIN(EXPANSION4,0);
	SetPIN(EXPANSION5,0);
	// Apagado de los dígitos 0,1,2,3,4 y 5

	// Encendido de los segmentos a, b, c, d, e, f y g
	// Selección del digito a encender ( cíclico en modulo 6 )

	SetPIN(segmento_a,msgDisplay[i] & 0x01);
	SetPIN(segmento_b,(msgDisplay[i]>>1) & (0x01));
	SetPIN(segmento_c,(msgDisplay[i]>>2) & (0x01));
	SetPIN(segmento_d,msgDisplay[i]>>3 & 0x01);
	SetPIN(segmento_e,msgDisplay[i]>>4 & 0x01);
	SetPIN(segmento_f,msgDisplay[i]>>5 & 0x01);
	SetPIN(segmento_g,msgDisplay[i]>>6 & 0x01);

	switch(i){
		
		case 0:
			SetPIN(EXPANSION0,1);
			break;
		case 1:
			SetPIN(EXPANSION1,1);
			break;
		case 2:
			SetPIN(EXPANSION2,1);
			break;
		case 3:
			SetPIN(EXPANSION3,1);
			break;
		case 4:
			SetPIN(EXPANSION4,1);
			break;
		case 5:
			SetPIN(EXPANSION5,1);
			break;
		default: return;
	}
	i++;
	i%=6;
	}

