#include "Aplicacion.h"

void Inicializar_Display7s( void )
{
	SetPINSEL(EXPANSION0, PINSEL_GPIO); 
	SetPINSEL(EXPANSION1, PINSEL_GPIO); 
	SetPINSEL(EXPANSION2, PINSEL_GPIO); 
	SetPINSEL(EXPANSION3, PINSEL_GPIO); 
	SetPINSEL(EXPANSION4, PINSEL_GPIO); 
	SetPINSEL(EXPANSION5, PINSEL_GPIO); 
	SetPINSEL(EXPANSION6, PINSEL_GPIO); 
	SetPINSEL(EXPANSION7, PINSEL_GPIO); 
	SetPINSEL(EXPANSION8, PINSEL_GPIO); 
	SetPINSEL(EXPANSION9, PINSEL_GPIO); 
	SetPINSEL(EXPANSION10, PINSEL_GPIO); 
	SetPINSEL(EXPANSION11, PINSEL_GPIO); 
	SetPINSEL(EXPANSION12, PINSEL_GPIO); 
	SetPINSEL(EXPANSION13, PINSEL_GPIO); 
	SetPINSEL(EXPANSION14, PINSEL_GPIO); 
	SetPINSEL(EXPANSION15, PINSEL_GPIO); 

	SetDIR(EXPANSION0, SALIDA);
	SetDIR(EXPANSION1, SALIDA);
	SetDIR(EXPANSION2, SALIDA);
	SetDIR(EXPANSION3, SALIDA);
	SetDIR(EXPANSION4, SALIDA);
	SetDIR(EXPANSION5, SALIDA);
	SetDIR(EXPANSION6, SALIDA);
	SetDIR(EXPANSION7, SALIDA);
	SetDIR(EXPANSION8, SALIDA);
	SetDIR(EXPANSION9, SALIDA);
	SetDIR(EXPANSION10, SALIDA);
	SetDIR(EXPANSION11, SALIDA);
	SetDIR(EXPANSION12, SALIDA);
	SetDIR(EXPANSION13, SALIDA);
	SetDIR(EXPANSION14, SALIDA);
	SetDIR(EXPANSION15, SALIDA);

	SetPINMODE_OD(EXPANSION0, 0);
	SetPINMODE_OD(EXPANSION1, 0);
	SetPINMODE_OD(EXPANSION2, 0);
	SetPINMODE_OD(EXPANSION3, 0);
	SetPINMODE_OD(EXPANSION4, 0);
	SetPINMODE_OD(EXPANSION5, 0);
	SetPINMODE_OD(EXPANSION6, 0);
	SetPINMODE_OD(EXPANSION7, 0);
	SetPINMODE_OD(EXPANSION8, 0);
	SetPINMODE_OD(EXPANSION9, 0);
	SetPINMODE_OD(EXPANSION10, 0);
	SetPINMODE_OD(EXPANSION11, 0);
	SetPINMODE_OD(EXPANSION12, 0);
	SetPINMODE_OD(EXPANSION13, 0);
	SetPINMODE_OD(EXPANSION14, 0);
	SetPINMODE_OD(EXPANSION15, 0);
	
	// Configurar salidas
	// elección de GPIO - PINSEL
	// a, b, c, d, e, f, g, dp
	// común dígito 0, común dígito 1, común dígito 2
	// común dígito 3, común dígito 4, común dígito 5

	// Configuración de los puertos - FIODIR
	// a, b, c, d, e, f, g, dp
	// común dígito 0, común dígito 1, común dígito 2
	// común dígito 3, común dígito 4, común dígito 5

}
int Inicializar_Teclado(void){

}
