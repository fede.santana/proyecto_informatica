/*
 * Aplicacion.h
 *
 *  Created on: 14/06/2013
 *      Author: Marcelo
 */

#ifndef APLICACION_H_
#define APLICACION_H_

	#include "RegsLPC1769.h"
	#include "KitInfo2_BaseBoard.h"
	#include "FW_Display-Expansion2_DIsplayx6.h"
	#include "FW_GPIO.h"


	void InicializarKit ( void );
	void my_Display (uint32_t);
	int Tecla(void);
	void InicializarTeclado(void);
#endif /* APLICACION_H_ */
