#include <gpio.h>

void SetPINSEL( uint8_t port , uint8_t pin ,uint8_t sel){
	port = port * 2 + pin / 16;
	pin = ( pin % 16 ) * 2;
	PINSEL[ port ] = PINSEL[ port ] & ( ~ ( 3 << pin ) );
	PINSEL[ port ] = PINSEL[ port ] | ( sel << pin );
}

void SetPINMODE( uint8_t port , uint8_t pin ,uint8_t modo)
{
	port = port * 2 + pin / 16;
	pin = ( pin % 16 ) * 2;
	PINMODE[ port ] = PINMODE[ port ] & ( ~ ( 3 << pin ) );
	PINMODE[ port ] = PINMODE[ port ] | ( modo << pin );
}
void SetMODE ( uint8_t puerto , uint8_t bit , uint8_t modo )
{
	/*
	* Cuando es entrada se puede configurar los siguientes modos:
	* PULL_UP , PULL_DOWN, REPEATER_MODE, AIRE.
	*
	*/
	__RW uint32_t *p = ( __RW uint32_t * )  0x4002C040 ; 

	*( p + puerto * 2 + bit / 16 ) = *( p + puerto * 2 + bit / 16 ) & ~( 0x3 << (2 * bit));
	*( p + puerto * 2 + bit / 16 ) = *( p + puerto * 2 + bit / 16 ) | ( modo << (2 * (bit % 16)));

}
void SetMODE_OD( uint8_t port , uint8_t pin , uint8_t dir )
{

	PINMODE_OD[ port ] = PINMODE_OD[ port ] & ( ~ ( 1 << pin ) );
	PINMODE_OD[ port ] = PINMODE_OD[ port ] | ( dir << pin );

}


void SetDIR( uint8_t port , uint8_t pin , uint8_t dir )
{
	port = port * 8; // Porque saltea espacios, ver bloque memoria.
	GPIOs[ port ] = GPIOs[ port ] & ( ~ ( 1 << pin ) );
	GPIOs[ port ] = GPIOs[ port ] | ( dir << pin );
}

void SetPIN( uint8_t port , uint8_t pin , uint8_t estado )
{
	port = port * 8 + 5; // Porque saltea espacios, ver bloque memoria.
	GPIOs[ port ] = GPIOs[ port ] & ( ~ ( 1 << pin ) );
	GPIOs[ port ] = GPIOs[ port ] | ( estado << pin );
}

uint8_t GetPIN( uint8_t port , uint8_t pin , uint8_t actividad )
{
	port = port * 8 + 5;
	return ( ( ( GPIOs[ port ] >> pin ) & 1 ) == actividad ) ? 1 : 0;
}
