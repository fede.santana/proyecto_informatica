/*
===============================================================================
 Name        : PruebadeMyTimer.c
 Author      : Ari
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include <MyTimer.h>

static int flag=0;

int main ()
{
	initLed();
	initPLL();
	InitTimer0();

	while(1)
	{
		if (flag == 1)
		{
			if(GetPIN(0,22,0))
			{
				SetPIN(0,22,1);
				flag = 0;
			}

			else
			{
				SetPIN(0,22,0);
				flag = 0;
			}
		}
	}
return 0;
}
void InitTimer0(void)
{

	PCONP		|= (1<<1);
	PCLKSEL0 	|= (1<<2);
	T0PR 		= PR;
	T0MR0 		= 10000000;
	T0CTCR 		&= TIMER;
	T0MCR 		&= CLR_MTCH_CONFIG;
	T0MCR 		|= (1<<0);
	T0MCR 		|= (1<<1);
	T0TCR 		&= CLEAR_RST_EN;
	T0TCR 		|= (1<<1);
	T0TCR 		&= TIMER_RST_OFF;
	T0TCR 		|= (1<<0);
	ISER0 		|= (1<<1);
}

void TIMER0_IRQHandler(void)
{
		if((T0IR == MR0))
			{
				T0IR |= MR0;
				flag = 1;
			}
}
void initPLL(void)
{
	SCS       = SCS_Value;

	if (SCS_Value & (1 << 5))               /* If Main Oscillator is enabled      */
		while ((SCS & (1<<6)) == 0);/* Wait for Oscillator to be ready    */

	CCLKCFG   = CCLKCFG_Value;      /* Setup Clock Divider                */

	PCLKSEL0  = PCLKSEL0_Value;     /* Peripheral Clock Selection         */
	PCLKSEL1  = PCLKSEL1_Value;

	CLKSRCSEL = CLKSRCSEL_Value;    /* Select Clock Source for PLL0       */

	PLL0CFG   = PLL0CFG_Value;      /* configure PLL0                     */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	PLL0CON   = 0x01;             /* PLL0 Enable                        */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & (1<<26)));/* Wait for PLOCK0                    */

	PLL0CON   = 0x03;             /* PLL0 Enable & Connect              */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & ((1<<25) | (1<<24))));/* Wait for PLLC0_STAT & PLLE0_STAT */

	PLL1CFG   = PLL1CFG_Value;
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	PLL1CON   = 0x01;             /* PLL1 Enable                        */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & (1<<10)));/* Wait for PLOCK1                    */

	PLL1CON   = 0x03;             /* PLL1 Enable & Connect              */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & ((1<< 9) | (1<< 8))));/* Wait for PLLC1_STAT & PLLE1_STAT */

	PCONP     = PCONP_Value;        /* Power Control for Peripherals      */

	CLKOUTCFG = CLKOUTCFG_Value;    /* Clock Output Configuration         */

	FLASHCFG  = (FLASHCFG & ~0x0000F000) | FLASHCFG_Value;
}

void initLed(void)
{
	SetPINSEL(0,22,0);
	SetMODE_OD(0,22,0);
	SetDIR( 0, 22, 1);
	SetPIN(0,22,0);
}
