# Cámara controlada por 2 motores.

[![VSCode](https://img.shields.io/badge/vscode-blue?logo=visual-studio-code&style=for-the-badge)](https://code.visualstudio.com/)
[![Arduino](https://img.shields.io/badge/arduino-blue?visualstudiocode.svg-code&style=for-the-badge)](https://code.visualstudio.com/)
[![GPL License](https://img.shields.io/badge/license-lgpl__2__1-green)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
[![Release](https://img.shields.io/badge/release-v1.0-blue)](https://gitlab.com/fede.santana/proyecto_informatica/blob/master/Release/RELEASE.md)
* * *

### Download
¡Descarga y ejecuta en el NPX lo que sea!. 
* * *

### What's in the download?

Deberíamos poner algo así chicos: 

```
webslides/
├── index.html
├── css/
│   ├── base.css
│   └── colors.css
│   └── svg-icons.css (optional)
├── js/
│   ├── webslides.js
│   └── svg-icons.js (optional)
└── demos/
└── images/
```

## Etapas!

- Acá iría la imagen de las etapas jeje

## Features

- Control de motores paso a paso con Arduino!!
- Aca vamos poniendo las actualizaciones nuevas

## Markup

- Acá podemos decir algo del codigo

```html
<article id="webslides">
    <section>
        <h1>Slide 1</h1>
    </section>
    <section class="bg-black aligncenter">
    <!-- .wrap = container 1200px -->
        <div class="wrap">
            <h1>Slide 2</h1>
        </div>
    </section>
</article>
```

### Velocidad motores? qsy

```html
<article id="webslides" class="vertical">
```

### Extensions

You can add:

- [Unsplash](https://unsplash.com) photos
- [animate.css](https://daneden.github.io/animate.css)
- [particles.js](https://github.com/VincentGarreau/particles.js)
- [Animate on scroll](http://michalsnik.github.io/aos/) (Useful for longform articles)
- [pt](http://williamngan.github.io/pt/)

### Dive In!

- Do not miss [our demos](https://webslides.tv/). 
- Want to get techie? Read [our wiki](wiki):
  - [FAQ](https://github.com/webslides/WebSlides/wiki)
  - [Core API](https://github.com/webslides/WebSlides/wiki/Core-API)
  - [Plugin Docs](https://github.com/webslides/WebSlides/wiki/Plugin-docs)
  - [Plugin Development](https://github.com/webslides/WebSlides/wiki/Plugin-development)
 
### Credits

- Motorsites created by [@fede.santana](https://gitlab.com/fede.santana/proyecto_informatica) using [Infotronic](http://www2.electron.frba.utn.edu.ar/archivos/infotronic.pdf).

