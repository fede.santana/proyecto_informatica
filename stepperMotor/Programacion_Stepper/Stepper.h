/*
* Basada en Stepper.cpp - Stepper library for Wiring/Arduino - Version 1.1.0
*
* The sequence of control signals for 4 control wires is as follows:
*
* Step C0 C1 C2 C3
*    1  1  0  1  0
*    2  0  1  1  0
*    3  0  1  0  1
*    4  1  0  0  1
*/
#include <gpio.h>
typedef struct fourWireStepper{
    int coilPinA[2]; // Bobina 1
    int coilPinB[2]; // Bobina 1
    int coilPinC[2]; // Bobina 2
    int coilPinD[2]; // Bobina 2
    int maxSteps; // total number of steps for this motor
    unsigned long stepDelay; // delay between steps, in ms, based on speed
    int currentStep;    // which step the motor is on
    int direction;      // motor direction
    unsigned long stepTimeStamp; // time stamp in us of the last step taken
} fourWireStepper_t;
int decodePin(int expPin, fourWireStepper_t *PaP,char Pin);
void setCoilPin(fourWireStepper_t* PaP);
int Stepper(int coilPinA, int coilPinB, int coilPinC, int coilPinD, int maxSteps, int whatSpeed, fourWireStepper_t* PaP);
void step(int steps_to_move, fourWireStepper_t* PaP);
void stepMotor(int thisStep, fourWireStepper_t* PaP);
void MyTimer (void);