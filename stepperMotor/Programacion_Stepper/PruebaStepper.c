/*
===============================================================================
 Name        : PruebaStepper.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "Stepper.h"
// DEFAULTS CONFIGURATION.

#define NUMBER_OF_STEPS 60
#define PIN1A 16
#define PIN1B 15
#define PIN2A 14
#define PIN2B 13
#define SPEED 50

fourWireStepper_t PaP;

int main(void)
{
    // Aca deberíamos inicializar lo necesario de la placa.
	Stepper(PIN1A, PIN1B, PIN2A, PIN2B, NUMBER_OF_STEPS, SPEED, &PaP);
	SetPINSEL ( 0,18 ,0);
	SetDIR ( 0,18 , ENTRADA );
	SetMODE(0,18,0);
    volatile static int i=0;
    static int buttonFlag = 1;
    while (1)
    {
    if(buttonFlag)
    	if(GetPIN(0,18,0)){
    	buttonFlag=0;
    	i++;
    }
    if(!(GetPIN(0,18,0))) buttonFlag=1;

    if(i==4)i=0;
    stepMotor(i,&PaP);
    }
    return 0;
}
