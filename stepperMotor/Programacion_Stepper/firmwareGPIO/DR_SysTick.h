
#ifndef DR_SYSTICK_H_
#define DR_SYSTICK_H_

#include "Tipos.h"

#define TICKUS	2500	//Microsegundos por tick

#define SYSTICK ((__RW systick_t*) 0xE000E010UL)	//34.4.4 System timer, SysTick

typedef struct{
	union{
		__RW uint32_t _STCTRL;
		struct{
			__RW uint32_t _ENABLE:1;
			__RW uint32_t _TICKINT:1;
			__RW uint32_t _CLKSOURCE:1;
			__RW uint32_t _RESERVED_0:14;
			__RW uint32_t _COUNTFLAG:1;
			__RW uint32_t _RESERVED_1:14;
		};
	};
	__RW uint32_t _STLOAD;
	__RW uint32_t _STVAL;
	__R uint32_t _STCALIB;
}systick_t;

#define STCTRL	SYSTICK->_STCTRL
	#define STENABLE	SYSTICK->_ENABLE
	#define STTICKINT	SYSTICK->_TICKINT
	#define STCLKSOURCE	SYSTICK->_CLKSOURCE
	#define STCOUTFLAG	SYSTICK->_COUNTFLAG
#define STLOAD	SYSTICK->_STLOAD
#define STVAL	SYSTICK->_STVAL
#define STCALIB	SYSTICK->_STCALIB


void Inicializacion_SysTick();
unsigned long millis_systick();

#endif /* DR_SYSTICK_H_ */
