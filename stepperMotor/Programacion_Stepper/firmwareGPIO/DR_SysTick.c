
#include "DR_SysTick.h"
#include "DR_Timers.h"
#include "DR_LCD.h"

unsigned long time_since_init = 0;

/**
 * @fn			void Inicializacion_SysTick (void)
 * @brief		Inicializa el contador del sistema
 * @return				void
 */
void Inicializacion_SysTick(){
	STLOAD = (STCALIB/4) - 1;	//For a period of N clock cycles, use a RELOAD value of N-1.
	STVAL = 0;

	STENABLE = 1;	//Enables the counter
	STTICKINT = 1;	//1 = counting down to zero to asserts the SysTick exception request
	STCLKSOURCE = 1;//1 = processor clock.

	return;
}

/**
 * @fn			void SysTick_Handler (void)
 * @brief		Funcion manejadora del contador del sistema
 * @return				void
 */
void SysTick_Handler(){
	time_since_init++;
	AnalizarTimers();
	Dato_LCD();
}

/**
 * @fn			void millis_systick (void)
 * @brief		Devuelve la cantidad de milisegundos transcurridos desde la inicializacion
 * @return		Cantidad de milisegundos (unsigned long)
 */
unsigned long millis_systick(){
	return time_since_init*2.5;
}
