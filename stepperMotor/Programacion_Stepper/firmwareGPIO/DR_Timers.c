
#include "DR_Timers.h"

volatile timer_t timers[CANT_TIMERS];

/**
\fn void AnalizarTimers ( void )
\brief 	Decremento periódico de los contadores
\details 	Decrementa los contadores de los timers en ejecución.
\details 	Debe ser llamada periódicamente con la base de tiempos
\return 	void
*/
void AnalizarTimers(){

	for(uint8_t i = 0; i < CANT_TIMERS; ++i) {

		if(timers[i].state == RUN) {

			if(timers[i].runTime > 0)
				timers[i].runTime--;

			if(timers[i].runTime == 0) {
				timers[i].finish = 1;
				timers[i].state = FINISH;
			}
		}
	}
}
