
#ifndef DR_TIMERS_H_
#define DR_TIMERS_H_

#include "Tipos.h"

#define CANT_TIMERS 16

#define CENTECIMAS	4 		//4 ticks es una centesima de segundo
#define DECIMAS		40 		//40 ticks es una decima de segundo
#define SEGUNDOS 	400 	//400 ticks son un segundo
#define MINUTOS	 	2400 	//2400 ticks son un minuto

enum{FINISH=0, STANDBY, RUN};

typedef void (*Timer_Handler)(void);

typedef struct{
	uint32_t runTime;
	uint8_t  finish;
	Timer_Handler handler;
	uint8_t state;
	uint8_t base;
}timer_t;

extern volatile timer_t timers[CANT_TIMERS];

void AnalizarTimers();

#endif /* DR_TIMERS_H_ */
