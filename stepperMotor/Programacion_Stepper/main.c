#include "Stepper.h"

#define PINA 14
#define PINB 15
#define PINC 16
#define PIND 17
#define MAXSTEPS 200
#define VELOCIDAD 60

fourWireStepper_t PaP;

int main (void)
{
    Stepper (PINA, PINB, PINC, PIND, MAXSTEPS, VELOCIDAD, &PaP);

    step (20, &PaP);

    return 0;
}