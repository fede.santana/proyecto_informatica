
#ifndef TIPOS_H_
#define TIPOS_H_

typedef		unsigned int		uint32_t;
typedef		short unsigned int	uint16_t;
typedef		unsigned char		uint8_t ;
typedef		int					int32_t;
typedef		short int			int16_t;
typedef		char				int8_t;

#define 		__R					volatile const
#define 		__W					volatile
#define 		__RW				volatile

//#define NULL	0

#endif /* TIPOS_H_ */
