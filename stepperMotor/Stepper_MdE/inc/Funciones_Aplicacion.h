/*
 * Funciones_Aplicacion.h
 *
 *  Created on: Sep 25, 2019
 *      Author: fg
 */

#ifndef FUNCIONES_APLICACION_H_
#define FUNCIONES_APLICACION_H_
#include <Stepper.h>

//Estados mde principal
#define GOING_HOME			0
#define READY 				1
#define MOVING				2

int youAreDrunkGoHome(fourWireStepper_t* PaP_func);
int moveRight(int qtySteps,fourWireStepper_t* PaP_func);
int moveLeft(int qtySteps,fourWireStepper_t* PaP_func);
int moveRightMax(fourWireStepper_t* PaP_func);
int moveLeftMax(fourWireStepper_t* PaP_func);
int moveRightMin(fourWireStepper_t* PaP_func);
int moveLeftMin(fourWireStepper_t* PaP_func);


#endif /* FUNCIONES_APLICACION_H_ */
