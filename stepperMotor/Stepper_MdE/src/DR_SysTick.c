/*******************************************************************************************************************************//**
 *
 * @file		DR_SysTick.c
 * @brief		Descripcion del modulo
 * @date		4 de may. de 2016
 * @author		Ing. Marcelo Trujillo
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include "DR_Timers.h"
#include "DR_SysTick.h"
#include <gpio.h>
unsigned long volatile miliSegundo = 6;
unsigned long volatile time_since_init = 0;
unsigned int volatile flag = 0;
int volatile thisStep = 0;
/*
 **** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/
/** @fn void SysTickInic ( void )
 * @details Inicializacion del systick
 * @details No Portable
 * @param 	void
 * @return 	void.
 */
void InicializarSysTick ( void )
{
	STRELOAD = ( STCALIB / 10) - 1;
	STCURR = 0;

	CLKSOURCE = 1;
	ENABLE = 1;
	TICKINT = 1;
}

void SysTick_Handler(void)
{
	miliSegundo--;
	miliSegundo%=100; //Proteger para que la variable solo tome valores entre 0 y 6
}
