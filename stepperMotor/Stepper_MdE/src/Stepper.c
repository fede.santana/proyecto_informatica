#include <Stepper.h>
#include <gpio.h>
#include <LPC1769Regs.h>
#include <Funciones_Aplicacion.h>

//Defines parametros de PaP
#define PINA 13
#define PINB 14
#define PINC 15
#define PIND 16
#define MAXSTEPS 200
#define VELOCIDAD 60
static int numberSteps=0;
extern fourWireStepper_t PaP;
extern uint8_t flagLimitSwitch;

void offCoils (void);

void offCoils ()
{
	SetPIN(PaP.coilPinA[0],PaP.coilPinA[1], LOW);
	SetPIN(PaP.coilPinB[0],PaP.coilPinB[1], LOW);
	SetPIN(PaP.coilPinC[0],PaP.coilPinC[1], LOW);
	SetPIN(PaP.coilPinD[0],PaP.coilPinD[1], LOW);
}

int Stepper(int coilPinA, int coilPinB, int coilPinC, int coilPinD, int maxSteps, int whatSpeed, fourWireStepper_t *PaP)
{

    // Inicialización variables

    PaP->currentStep = 0;
    PaP->stepTimeStamp = 0;
    PaP->maxSteps = maxSteps;
    PaP->stepDelay = 60L * 1000L / maxSteps / whatSpeed;
    
    // Traduce el numero de Pin de Expansion a su correspondiente puerto y N° de Pin

    if(decodePin(coilPinA,PaP,'A')==-1) {
        return -1;
    } 
    if(decodePin(coilPinB,PaP,'B')==-1) { 
        return -1;
    }
    if(decodePin(coilPinC,PaP,'C')==-1) {
        return -1;
    }
    if(decodePin(coilPinD,PaP,'D')==-1) {
        return -1;
    }

    setCoilPin(PaP); // Se configuran los pines

    return 0; 
}

void step(int steps_to_move, fourWireStepper_t *PaP)
{
    static unsigned int steps_left=0;
    if(steps_to_move!=0){
    steps_left = abs(steps_to_move); // how many steps to take


    // determine direction based on whether steps_to_mode is + or -:
    if (steps_to_move >= 0)
    {
        PaP->direction = 1;
    }
    if (steps_to_move < 0)
    {
        PaP->direction = 0;
    }
    }
    // decrement the number of steps, moving one step each time:
    if (steps_left > 0)
    { 

    	extern unsigned long volatile miliSegundo; // Tiempo actual en microsegundos, función de Arduino.
        // move only if the appropriate delay has passed:
        if (miliSegundo == 0)
        {
            miliSegundo = 12;
            // increment or decrement the step number,
            // depending on direction:
            if (PaP->direction == 1)
            {
                PaP->currentStep++;
                if (PaP->currentStep == PaP->maxSteps)
                {
                    PaP->currentStep = 0;
                }
            }

            else
            {
                if (PaP->currentStep == 0)
                {
                    PaP->currentStep = PaP->maxSteps;
                }
                PaP->currentStep--;
            }
            // decrement the steps left:
            steps_left--;
            if(numberSteps>0) numberSteps--;
            else numberSteps++;
            if(steps_left < 0) steps_left = 0;
            // step the motor to step number 0, 1, ..., {3 or 10}
            stepMotor(PaP->currentStep % 4, PaP);
        }
    }

}

void stepMotor(int thisStep, fourWireStepper_t *PaP)
{
    switch (thisStep)
    {
    case 0: // 1010
    	SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], HIGH);
        SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], LOW);
        SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], HIGH);
        SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], LOW);
        break;
    case 1: // 0110
        SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], LOW);
        SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], HIGH);
        SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], HIGH);
        SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], LOW);
        break;
    case 2: //0101
        SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], LOW);
        SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], HIGH);
        SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], LOW);
        SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], HIGH);
        break;
    case 3: //1001
        SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], HIGH);
        SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], LOW);
        SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], LOW);
        SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], HIGH);
        break;
    }
}
int decodePin(int expPin, fourWireStepper_t *PaP,char Pin)
{
    //Traduce el numero de Pin de Expansion a su correspondiente puerto y N° de Pin
    int vector[2];
    switch (expPin)
    {
    case 16:
        vector[0] = 2;
        vector[1] = 8;
        break;
    case 15:
        vector[0] = 1;
        vector[1] = 18;
        break;
    case 14:
        vector[0] = 1;
        vector[1] = 21;
        break;
    case 13:
        vector[0] = 1;
        vector[1] = 24;
        break;
    default:
        return -1;
        break;
    }
    switch(Pin){
    case 'A':
    		PaP->coilPinA[0] = vector[0];
    		PaP->coilPinA[1] = vector[1];
    		break;
    case 'B':
        	PaP->coilPinB[0] = vector[0];
        	PaP->coilPinB[1] = vector[1];
        	break;
    case 'C':
        	PaP->coilPinC[0] = vector[0];
        	PaP->coilPinC[1] = vector[1];
        	break;
    case 'D':
        	PaP->coilPinD[0] = vector[0];
        	PaP->coilPinD[1] = vector[1];
        	break;
    }
    return 0;
}
void setCoilPin(fourWireStepper_t *PaP){
    /*
    * SetPINSEL: Configura la función del PIN.
    * SetDIR: Soy OUTPUT o INPUT.
    * SetMODE_OD: Sólo cuando lo configure como salida, ¿qué tipo de salida? PUSH_PULL u OPEN_DRAIN
    */
    //Configuracion del Pin A
    SetPINSEL(PaP->coilPinA[0], PaP->coilPinA[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinA[0], PaP->coilPinA[1], OUTPUT);
    SetMODE_OD(PaP->coilPinA[0], PaP->coilPinA[1], PUSH_PULL);
    //Configuracion del Pin B
    SetPINSEL(PaP->coilPinB[0], PaP->coilPinB[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinB[0], PaP->coilPinB[1], OUTPUT);
    SetMODE_OD(PaP->coilPinB[0], PaP->coilPinB[1], PUSH_PULL);
    //Configuracion del Pin C
    SetPINSEL(PaP->coilPinC[0], PaP->coilPinC[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinC[0], PaP->coilPinC[1], OUTPUT);
    SetMODE_OD(PaP->coilPinC[0], PaP->coilPinC[1], PUSH_PULL);
    //Configuracion del Pin D
    SetPINSEL(PaP->coilPinD[0], PaP->coilPinD[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinD[0], PaP->coilPinD[1], OUTPUT);
    SetMODE_OD(PaP->coilPinD[0], PaP->coilPinD[1], PUSH_PULL);

    return;
}
void InicializarLimitSwitch(void){
	SetPINSEL(2,11,PINSEL_FUNC1);
	EXTMODE |= (0x0F << 0); //Todas por flanco
	EXTPOLAR &= ~(0x0F << 0);//Todas por flanco descendiente
	ISER0 |= (0x01 << 19); //Set Enable Eint1 (Entrada digital2)
}
void EINT1_IRQHandler (void)
{

	EXTINT = EXTINT | (0x01<<EINT1);			//!< Limpia Flag interrupcion
	flagLimitSwitch = 1;								//!< Set Flag final de carrera

}
void InitAll(void){
    InicializarPLL();
	InicializarSysTick();
    Stepper(PINA, PINB, PINC, PIND, MAXSTEPS, VELOCIDAD, &PaP);
    InicializarLimitSwitch();
}
void MDE_Stepper(){

	static volatile int estadoStepper = GOING_HOME; //volatile agrego fede

	if(flagLimitSwitch){
		step(25,&PaP);
		numberSteps = 25;
		flagLimitSwitch=0;
		estadoStepper = MOVING;
	}

	switch(estadoStepper){
		case GOING_HOME:
				youAreDrunkGoHome(&PaP);
				break;
		case READY:
				offCoils();
				if(numberSteps){
					step(numberSteps,&PaP);
					estadoStepper = MOVING;
				}
				break;
		case MOVING:

				if(!numberSteps){
					estadoStepper = READY;
				}
				if(numberSteps){
					step(0,&PaP);
				}
				break;
		default:
				estadoStepper = GOING_HOME;
				break;
		}
}
