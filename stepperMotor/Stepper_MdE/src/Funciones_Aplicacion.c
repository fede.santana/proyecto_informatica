/*
 * Funciones_Aplicacion.c
 *
 *  Created on: Sep 25, 2019
 *      Author: fg
 */
#include <Funciones_Aplicacion.h>

#define RETROCESO -20

int youAreDrunkGoHome(fourWireStepper_t* PaP_func){
	step(RETROCESO, PaP_func);
	return 0;
}

int moveRight(int qtySteps,fourWireStepper_t* PaP_func);
int moveLeft(int qtySteps,fourWireStepper_t* PaP_func);
int moveRightMax(fourWireStepper_t* PaP_func);
int moveLeftMax(fourWireStepper_t* PaP_func);
int moveRightMin(fourWireStepper_t* PaP_func);
int moveLeftMin(fourWireStepper_t* PaP_func);
