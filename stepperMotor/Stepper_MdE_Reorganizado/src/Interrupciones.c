/* INTERRUPCIONES DEL SISTEMA*/
#include <DR_tipos.h>
#include <gpio.h>
#include <Stepper.h>
extern unsigned long volatile miliSegundo;
extern uint8_t flagLimitSwitch;

void SysTick_Handler(void)
{
	miliSegundo--;
	miliSegundo%=6; //Proteger para que la variable solo tome valores esperados.
}

void EINT1_IRQHandler (void)
{

	EXTINT = EXTINT | (0x01<<EINT1);			//!< Limpia Flag interrupcion
	flagLimitSwitch = 1; //!< Set Flag final de carrera
}
