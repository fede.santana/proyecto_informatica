#include <Stepper.h>
#include <gpio.h>
#include <LPC1769Regs.h>
#include <Funciones_Aplicacion.h>



extern fourWireStepper_t PaP;
extern uint8_t flagLimitSwitch;

static int numberSteps = 0;

void MDE_Stepper(){

	static volatile int estadoStepper = GOING_HOME;

	if(flagLimitSwitch){
		step(25,&PaP);
		numberSteps = 25;
		flagLimitSwitch=0;
		estadoStepper = MOVING;
	}

	switch(estadoStepper){
		case GOING_HOME:
				youAreDrunkGoHome(&PaP);
				break;
		case READY:
				//offCoils(&PaP);
				SetPIN(PaP.coilPinA[0],PaP.coilPinA[1], LOW);
				SetPIN(PaP.coilPinB[0],PaP.coilPinB[1], LOW);
				SetPIN(PaP.coilPinC[0],PaP.coilPinC[1], LOW);
				SetPIN(PaP.coilPinD[0],PaP.coilPinD[1], LOW);
				if(numberSteps){
					step(numberSteps,&PaP);
					estadoStepper = MOVING;
				}
				break;
		case MOVING:

				if(!numberSteps){
					estadoStepper = READY;
				}
				if(numberSteps){
					step(0,&PaP);
				}
				break;
		default:
				estadoStepper = GOING_HOME;
				break;
		}
}
