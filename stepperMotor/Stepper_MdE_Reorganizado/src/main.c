/*
===============================================================================
 Name        : STEPPER_TEST_TIMER_ROBADO.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <Stepper.h>
#include <DR_Timers.h>
#include <PLL.h>
volatile fourWireStepper_t PaP; //Fede Agrego Volatile.
volatile uint8_t flagLimitSwitch=0;

int main (void)
{

	InitAll();
 //Apagar el led como control de carga
    SetPINSEL(2,1, PINSEL_GPIO);
    SetDIR(2,1, SALIDA);
    SetMODE_OD(2,1, PUSH_PULL);
    SetPIN(2,1,0);

    while (1)
    {
    	MDE_Stepper();
    }

    return 0;
}
