################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Funciones_Aplicacion.c \
../src/Inicializacion.c \
../src/Interrupciones.c \
../src/Stepper.c \
../src/cr_startup_lpc175x_6x.c \
../src/crp.c \
../src/gpio.c \
../src/main.c \
../src/mdE.c 

OBJS += \
./src/Funciones_Aplicacion.o \
./src/Inicializacion.o \
./src/Interrupciones.o \
./src/Stepper.o \
./src/cr_startup_lpc175x_6x.o \
./src/crp.o \
./src/gpio.o \
./src/main.o \
./src/mdE.o 

C_DEPS += \
./src/Funciones_Aplicacion.d \
./src/Inicializacion.d \
./src/Interrupciones.d \
./src/Stepper.d \
./src/cr_startup_lpc175x_6x.d \
./src/crp.d \
./src/gpio.d \
./src/main.d \
./src/mdE.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -D__REDLIB__ -I"/home/fg/Documents/MCUXpresso_11.0.0_2516/workspace/Stepper_MdE/inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


