/*
===============================================================================
 Name        : STEPPER_TEST_TIMER_ROBADO.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <Stepper.h>
#include <DR_Timers.h>
#include <PLL.h>
void InicializarSysTick (void);

#define PINA 13
#define PINB 14
#define PINC 15
#define PIND 16
#define MAXSTEPS 200
#define VELOCIDAD 60

fourWireStepper_t PaP;


int main (void)
{
    InicializarPLL();
	InicializarSysTick();
    Stepper (PINA, PINB, PINC, PIND, MAXSTEPS, VELOCIDAD, &PaP);
 //Apagar el led como control de carga
    SetPINSEL(2,1, PINSEL_GPIO);
    SetDIR(2,1, SALIDA);
    SetMODE_OD(2,1, PUSH_PULL);
    while (1)
    {
    	step (180, &PaP);
    }

    return 0;
}
