################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/DR_SysTick.c \
../src/DR_Timers.c \
../src/PLL.c \
../src/PR_Timers.c \
../src/Stepper.c \
../src/cr_startup_lpc175x_6x.c \
../src/crp.c \
../src/gpio.c \
../src/main.c 

OBJS += \
./src/DR_SysTick.o \
./src/DR_Timers.o \
./src/PLL.o \
./src/PR_Timers.o \
./src/Stepper.o \
./src/cr_startup_lpc175x_6x.o \
./src/crp.o \
./src/gpio.o \
./src/main.o 

C_DEPS += \
./src/DR_SysTick.d \
./src/DR_Timers.d \
./src/PLL.d \
./src/PR_Timers.d \
./src/Stepper.d \
./src/cr_startup_lpc175x_6x.d \
./src/crp.d \
./src/gpio.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -D__REDLIB__ -I"/home/federico/Desktop/proyecto_informatica/stepperMotor/Stepper_Anda/inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


