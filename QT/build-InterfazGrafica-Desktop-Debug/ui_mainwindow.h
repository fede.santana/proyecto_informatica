/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionConfigure;
    QWidget *centralWidget;
    QPushButton *bIzq;
    QPushButton *Centrar_en_Y;
    QPushButton *Centrar_en_X_3;
    QPushButton *bArr;
    QPushButton *bAba;
    QPushButton *bDer;
    QPushButton *Centrar_en_Y_2;
    QStatusBar *statusBar;
    QToolBar *toolBar;
    QMenuBar *menuBar;
    QMenu *menuMENU;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(436, 288);
        QPalette palette;
        MainWindow->setPalette(palette);
        MainWindow->setAcceptDrops(true);
        MainWindow->setWindowOpacity(0);
        MainWindow->setStyleSheet(QStringLiteral(""));
        actionConnect = new QAction(MainWindow);
        actionConnect->setObjectName(QStringLiteral("actionConnect"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/images/connect.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConnect->setIcon(icon);
        actionDisconnect = new QAction(MainWindow);
        actionDisconnect->setObjectName(QStringLiteral("actionDisconnect"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/images/images/disconnect.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDisconnect->setIcon(icon1);
        actionConfigure = new QAction(MainWindow);
        actionConfigure->setObjectName(QStringLiteral("actionConfigure"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/images/images/settings.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConfigure->setIcon(icon2);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAutoFillBackground(true);
        bIzq = new QPushButton(centralWidget);
        bIzq->setObjectName(QStringLiteral("bIzq"));
        bIzq->setEnabled(true);
        bIzq->setGeometry(QRect(180, 60, 40, 61));
        bIzq->setMaximumSize(QSize(40, 80));
        QPalette palette1;
        bIzq->setPalette(palette1);
        bIzq->setAutoFillBackground(false);
        bIzq->setStyleSheet(QStringLiteral(""));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/images/images/Flechita (3. copia).png"), QSize(), QIcon::Normal, QIcon::Off);
        bIzq->setIcon(icon3);
        bIzq->setIconSize(QSize(60, 60));
        bIzq->setCheckable(false);
        bIzq->setAutoExclusive(false);
        bIzq->setFlat(true);
        Centrar_en_Y = new QPushButton(centralWidget);
        Centrar_en_Y->setObjectName(QStringLiteral("Centrar_en_Y"));
        Centrar_en_Y->setGeometry(QRect(10, 50, 131, 41));
        QPalette palette2;
        Centrar_en_Y->setPalette(palette2);
        Centrar_en_Y->setAcceptDrops(false);
        Centrar_en_Y->setAutoFillBackground(false);
        Centrar_en_Y->setStyleSheet(QStringLiteral(""));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/images/images/HomeY.png"), QSize(), QIcon::Normal, QIcon::Off);
        Centrar_en_Y->setIcon(icon4);
        Centrar_en_Y->setIconSize(QSize(30, 30));
        Centrar_en_Y->setCheckable(false);
        Centrar_en_Y->setFlat(true);
        Centrar_en_X_3 = new QPushButton(centralWidget);
        Centrar_en_X_3->setObjectName(QStringLiteral("Centrar_en_X_3"));
        Centrar_en_X_3->setGeometry(QRect(10, 87, 131, 41));
        QPalette palette3;
        Centrar_en_X_3->setPalette(palette3);
        Centrar_en_X_3->setAcceptDrops(false);
        Centrar_en_X_3->setAutoFillBackground(false);
        Centrar_en_X_3->setStyleSheet(QStringLiteral(""));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/images/images/HomeX.png"), QSize(), QIcon::Normal, QIcon::Off);
        Centrar_en_X_3->setIcon(icon5);
        Centrar_en_X_3->setIconSize(QSize(30, 30));
        Centrar_en_X_3->setCheckable(false);
        Centrar_en_X_3->setFlat(true);
        bArr = new QPushButton(centralWidget);
        bArr->setObjectName(QStringLiteral("bArr"));
        bArr->setGeometry(QRect(200, 10, 89, 41));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/images/images/Flechita.png"), QSize(), QIcon::Normal, QIcon::Off);
        bArr->setIcon(icon6);
        bArr->setIconSize(QSize(60, 60));
        bArr->setFlat(true);
        bAba = new QPushButton(centralWidget);
        bAba->setObjectName(QStringLiteral("bAba"));
        bAba->setGeometry(QRect(210, 90, 61, 41));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/images/images/Flechita (copia).png"), QSize(), QIcon::Normal, QIcon::Off);
        bAba->setIcon(icon7);
        bAba->setIconSize(QSize(60, 60));
        bAba->setFlat(true);
        bDer = new QPushButton(centralWidget);
        bDer->setObjectName(QStringLiteral("bDer"));
        bDer->setGeometry(QRect(240, 40, 51, 61));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/images/images/Flechita (otra copia).png"), QSize(), QIcon::Normal, QIcon::Off);
        bDer->setIcon(icon8);
        bDer->setIconSize(QSize(60, 60));
        bDer->setFlat(true);
        Centrar_en_Y_2 = new QPushButton(centralWidget);
        Centrar_en_Y_2->setObjectName(QStringLiteral("Centrar_en_Y_2"));
        Centrar_en_Y_2->setGeometry(QRect(210, 50, 42, 36));
        QPalette palette4;
        Centrar_en_Y_2->setPalette(palette4);
        Centrar_en_Y_2->setAcceptDrops(false);
        Centrar_en_Y_2->setAutoFillBackground(false);
        Centrar_en_Y_2->setStyleSheet(QStringLiteral(""));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/images/images/Home.png"), QSize(), QIcon::Normal, QIcon::Off);
        Centrar_en_Y_2->setIcon(icon9);
        Centrar_en_Y_2->setIconSize(QSize(30, 30));
        Centrar_en_Y_2->setCheckable(false);
        Centrar_en_Y_2->setFlat(true);
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 436, 20));
        menuMENU = new QMenu(menuBar);
        menuMENU->setObjectName(QStringLiteral("menuMENU"));
        MainWindow->setMenuBar(menuBar);

        toolBar->addAction(actionConnect);
        toolBar->addAction(actionDisconnect);
        toolBar->addAction(actionConfigure);
        menuBar->addAction(menuMENU->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MOTORES EN ACCION ahre", 0));
        actionConnect->setText(QApplication::translate("MainWindow", "Connect", 0));
        actionDisconnect->setText(QApplication::translate("MainWindow", "Disconnect", 0));
        actionConfigure->setText(QApplication::translate("MainWindow", "Configure", 0));
        bIzq->setText(QString());
        Centrar_en_Y->setText(QString());
        Centrar_en_X_3->setText(QString());
        bArr->setText(QString());
        bAba->setText(QString());
        bDer->setText(QString());
        Centrar_en_Y_2->setText(QString());
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
        menuMENU->setTitle(QApplication::translate("MainWindow", "MENU", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
