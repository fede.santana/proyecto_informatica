#ifndef VENTANA_H
#define VENTANA_H

#include <QMainWindow>
//#include <Seteador.h> //puede que en realidad ni siquiera sea necesario
#include <QSerialPort>
#include <QTranslator>
#include <QString>
#include <QSignalMapper>
#include <QList>



class QLabel;

namespace Ui {
class ventana;
}

QT_END_NAMESPACE


class ventana : public QMainWindow
{
    Q_OBJECT
    public:
        ventana(QWidget *parent = nullptr);
        ~ventana();
        //void fillPortsInfo(); esta la dejo en pasusa hasta saber que hace
    public slots:
        void enumerarPuertos();

    private slots:
        void openSerialPort()   ;
        void closeSerialPort()  ;
        void on_encenderPushButtom( QString );
        void showPortInfo ( void );


private:
        //void showStatusMessage(const QString &message); //esta envia un mensaje
        Ui::ventana *ui;
        QSerialPort *m_serial = nullptr; //como me voy a referir a mi SerialPort
        //QLabel *m_status = nullptr; //este por ahora no se bien para que se uso
        //Seteador *m_settings = nullptr; //este se va a colorear cuando incluyamos el seteador
        static quint16 serial_vendor_id  ; //para completar estos, ejecutar por linea de comando dmesg, una vez que pueda conectar el arduino
        static quint16 serial_product_id ;

        bool serial_is_avalible ;
        QString serial_port_name ;
        QSignalMapper *signalMapper;

        //QSerialPort *arduino ;

};

#endif // VENTANA_H
