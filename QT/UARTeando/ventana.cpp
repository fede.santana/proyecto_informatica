#include "ventana.h"
#include "ui_ventana.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QToolBar>
#include <QLabel>
#include <QMessageBox>
#include <QDebug>




static const char blankString[] = QT_TRANSLATE_NOOP("SettingsDialog", "N/A");

ventana::ventana (QWidget *parent) : QMainWindow(parent) ,ui(new Ui::ventana), m_serial(new QSerialPort(this))
{

    ui->setupUi(this);

    /* OPCIONES DE DISENO*/
    this->setFixedSize(375,275);
    ui->actionConnect->setStyleSheet("font-weight: default; color: black; background-color: lightGray;");
    ui->actionDisconnect->setStyleSheet("font-weight: default; color: black; background-color: lightGray;");
    /*Fin opciones dise;o*/

    serial_is_avalible = false ; // Inicializo Variable.

    ui->actionConnect->setEnabled(true); // Habilita el boton conectar.
    ui->actionDisconnect->setEnabled(false); // Deshabilita el

    signalMapper = new QSignalMapper(this);
    signalMapper->setMapping(ui->bIzq, QString("A"));
    signalMapper->setMapping(ui->bDer, QString("B"));
    signalMapper->setMapping(ui->bAba, QString("C"));
    signalMapper->setMapping(ui->bArr, QString("D"));

    connect(ui->bIzq, SIGNAL(clicked()),signalMapper, SLOT (map()));
    connect(ui->bDer, SIGNAL(clicked()),signalMapper, SLOT (map()));
    connect(ui->bAba, SIGNAL(clicked()),signalMapper, SLOT (map()));
    connect(ui->bArr, SIGNAL(clicked()),signalMapper, SLOT (map()));

    connect(ui->bActualizar ,       SIGNAL(clicked()), this, SLOT (enumerarPuertos()));
    connect( ui->actionConnect,     SIGNAL(clicked()), this, SLOT(openSerialPort())  );
    connect( ui->actionConnect,     SIGNAL(clicked()), this, SLOT(showPortInfo())  );
    connect( ui->actionDisconnect,  SIGNAL(clicked()), this, SLOT(closeSerialPort()) );

    connect(signalMapper, SIGNAL(mapped(QString)),this, SLOT(on_encenderPushButtom(QString)));

}

ventana::~ventana()
{
    delete ui;
}

void ventana::openSerialPort(){

    QString portName;
    int i = ui->comboBoxPuertos->currentIndex();
    portName = ui->comboBoxPuertos->itemData(i).toString();
    if (portName.isEmpty()) {
        QMessageBox::critical(this, QString::fromLatin1("Error de conexión"), QString::fromLatin1("Seleccione un puerto válido"));
        return;
    }

    m_serial = new QSerialPort(portName);
    //m_serial = new QSerialPort("/dev/ttyUSB0") ;

    if (!serial_is_avalible)	//Si no hay conexión activa
    {
        qDebug () << "Number of available ports: " << QSerialPortInfo::availablePorts().length();

        foreach (const QSerialPortInfo &serialPortInfo , QSerialPortInfo::availablePorts()) {
            qDebug () <<"Number of available ports: " << serialPortInfo.portName();

        //QMessageBox msgBox;
        //msgBox.setWindowTitle("Conectando...");
         //   QString("Quieres conectar a:\n Vendor ID: %1 \n Product ID: %2")
         //   .arg(serialPortInfo.vendorIdentifier()).arg(serialPortInfo.productIdentifier()));
        //msgBox.setText("Quieres conectar a: \n" );
        //msgBox.setStandardButtons(QMessageBox::Si | QMessageBox::No);
        //msgBox.setStandardButtons(QMessageBox::Yes);
        //msgBox.addButton(QMessageBox::No);
        //msgBox.setDefaultButton(QMessageBox::No);

        QMessageBox msgBox;
        msgBox.setWindowTitle("Conectando...");
        msgBox.setInformativeText("Do you want to save your changes?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);

        if(msgBox.exec() == QMessageBox::Yes){

            if (m_serial->open(QSerialPort::WriteOnly)) // Si lo puedo abrir:
            {

                qDebug () <<"[OPEN] Port info: " <<serialPortInfo.description();
                serial_is_avalible = true;

                m_serial->setBaudRate(QSerialPort::Baud9600);
                m_serial->setDataBits(QSerialPort::Data8);
                m_serial->setParity(QSerialPort::NoParity);
                m_serial->setStopBits(QSerialPort::OneStop);
                m_serial->setFlowControl(QSerialPort::NoFlowControl);

                ui->actionConnect->setStyleSheet("font-weight: bold; color: black; background-color: lightgreen;");
                ui->actionConnect->setText("CONECTADO");

                ui->actionConnect->setEnabled(false); // Habilita el boton conectar.
                ui->actionDisconnect->setEnabled(true); // Deshabilita el boton conectar.
            }
            else
            {
                qDebug("[OPEN] SERIAL PORT - NOT OPENED") ;
                qDebug() << "[OPEN] error code = " << m_serial->error();
                qDebug() << "[OPEN] error string = " << m_serial->errorString();
            }

        }
        else{
                closeSerialPort();
                QMessageBox::warning(this,"Port error","No se pudo conectar el micro") ;
            }
        }
    }
}

void ventana::closeSerialPort(){
    if ((serial_is_avalible = true)){
        m_serial->close();
        ui->actionConnect->setStyleSheet("font-weight: default; color: black; background-color: lightGray;");
        ui->actionConnect->setText("Conectar");
        qDebug () <<"Number of available ports: " <<QSerialPortInfo::availablePorts().length();
        ui->actionConnect->setEnabled(true); // Habilita el boton conectar.
        ui->actionDisconnect->setEnabled(false); // Deshabilita el
    }
    serial_is_avalible = false ;
}


void ventana::on_encenderPushButtom(const QString comando)
{
    QString mensaje;
    QByteArray first;
    QByteArray two;
    QByteArray three;

    first = "#" ;
    two = "A" ;
    three = "$" ;

    mensaje.append("#")     ;
    mensaje.append(comando) ;
    mensaje.append("$")     ;
    QByteArray msg;
    msg.append(mensaje);

    m_serial->write(first);
    m_serial->write(two);
    m_serial->write(three);

    //qDebug(&mensaje ) ;

    qDebug( mensaje.toLatin1());
    //qDebug(msg) ;
    /*
    QByteArray send = (  comando ).toLatin1();
    if ( serial_is   QString s = "value";_avalible ){
        m_serial->write (send);
    }else{
        qDebug ("[WRITE] No se pudo escribir el serial");
    }*/
}

void ventana::showPortInfo()
{
    if ( (serial_is_avalible=true) ){
        //ui->vidLabel->setText (tr("Vendor ID: %1").arg(serial_vendor_id) ) ;
        //ui->pidLabel->setText (tr("Product ID: %1").arg(serial_product_id) ) ;
    } else {
        //ui->vidLabel->setText (tr("Vendor ID: ") ) ;
        //ui->pidLabel->setText (tr("Product ID: ") ) ;
    }

    //const QStringList list = ui->serialPortInfoListBox->itemData(idx).toStringList();
    //ui->vidLabel->setText(tr("Description: %1").arg(serial_is_avalible=true ? serial_vendor_id :  tr(blankString)  ));
    //ui->pidLabel->setText(tr("Product Identifier: %1").arg(serial_is_avalible=true ?  serial_product_id :  tr(blankString) ));
    //ui->vidLabel->(serial_is_avalible=true ? setText( "Product Identifier: %hu " serial_vendor_id ) :   );

     //* con respecto a estos no estoy tan segura
    //ui->pidLabel->setText(tr("Description: %1").arg(list.count() > 1 ? list.at(1) : tr(blankString)));
     /*ui->manufacturerLabel->setText(tr("Manufacturer: %1").arg(list.count() > 2 ? list.at(2) : tr(blankString)));
    ui->serialNumberLabel->setText(tr("Serial number: %1").arg(list.count() > 3 ? list.at(3) : tr(blankString)));
    ui->locationLabel->setText(tr("Location: %1").arg(list.count() > 4 ? list.at(4) : tr(blankString)));*/


}

void ventana::enumerarPuertos()
{
    ui->comboBoxPuertos->clear();

    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();

    for (int i = 0; i < ports.size(); i++){
        ui->comboBoxPuertos->addItem(ports.at(i).portName(), ports.at(i).portName());
    }
}

