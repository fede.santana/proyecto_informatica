QT       += core gui serialport
QT += core gui widgets
QT += widgets serialport
QT += core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UARTeando
TEMPLATE = app

SOURCES += \
        main.cpp \
        ventana.cpp \

HEADERS += \
        ventana.h \

FORMS += \
        ventana.ui

RESOURCES += \
    imagenes.qrc




QMAKE_CXXFLAGS += -std=gnu++14

DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += c++11

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

