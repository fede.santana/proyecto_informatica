#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QToolBar>
#include <QLabel>
#include <QMessageBox>


QT_USE_NAMESPACE

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_status(new QLabel),
    m_settings(new SettingsDialog),
  //! [1]
      m_serial(new QSerialPort(this))
  //! [1]
{
     ui->setupUi(this);
     connect( ui->actionConnect, &QAction::triggered, this, &MainWindow::openSerialPort);
     connect( ui->actionDisconnect, &QAction::triggered, this, &MainWindow::closeSerialPort);
     connect( ui->bIzq , SIGNAL(clicked()) , this , SLOT(on_encenderPushButtom_clicked()) ) ;
     connect( ui->bDer , SIGNAL(clicked()) , this , SLOT(on_encenderPushButtom_clicked()) ) ;
     connect( ui->bAba , SIGNAL(clicked()) , this , SLOT(on_encenderPushButtom_clicked()) ) ;
     connect( ui->bArr , SIGNAL(clicked()) , this , SLOT(on_encenderPushButtom_clicked()) ) ;


     //m_shadowEffect = new QGraphicsEffect(this);
     //m_shadowEffect->setColor(QColor(0, 0, 0, 255 * 0.3));

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::openSerialPort()
{
    const SettingsDialog::Settings p = m_settings->settings();
    m_serial->setPortName(p.name);
    m_serial->setBaudRate(p.baudRate);
    m_serial->setDataBits(p.dataBits);
    m_serial->setParity(p.parity);
    m_serial->setStopBits(p.stopBits);
    m_serial->setFlowControl(p.flowControl);
    if (m_serial->open(QIODevice::ReadWrite)) {
        ui->actionConnect->setEnabled(false);
        ui->actionDisconnect->setEnabled(true);
        ui->actionConfigure->setEnabled(false);
        showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                          .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                          .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
    } else {
        QMessageBox::critical(this, tr("Error"), m_serial->errorString());

        showStatusMessage(tr("Open error"));
    }
}

void MainWindow::closeSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();
    //m_console->setEnabled(false);
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionConfigure->setEnabled(true);
    showStatusMessage(tr("Disconnected"));
}


void MainWindow::on_actionConfigure_triggered()
{
    m_settings->show();
}

void MainWindow::showStatusMessage(const QString &message)
{
    m_status->setText(message);
}

void MainWindow::on_encenderPushButtom_clicked()
{
    if ( m_serial->isWritable() ){
        m_serial->write ("255");//esto hay que ver porque lo saque del Arduino
    }
}

