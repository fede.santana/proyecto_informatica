#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <settingsdialog.h>
#include <QSerialPort>
//#include <QPalette>


class QLabel;

namespace Ui {
class MainWindow;
}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void fillPortsInfo();

private slots:

    void openSerialPort();
    void closeSerialPort() ;

    //void on_speedButton_clicked();
    //void on_stepsButton_clicked();
    //void on_pushButton_clicked();


    //void on_actionAbrir_config_triggered();

    //void on_actionConfig_triggered();

    void on_actionConfigure_triggered();
    //void confirmartion () ;

   void on_encenderPushButtom_clicked();



private:
    void showStatusMessage(const QString &message);

    Ui::MainWindow *ui;
    QLabel *m_status = nullptr;
    SettingsDialog *m_settings = nullptr;
    QSerialPort *m_serial = nullptr;
    //QColor lightCyan = QColor(Qt::cyan).light(180);
     //QGraphicsEffect *m_shadowEffect;

};

#endif // MAINWINDOW_H
