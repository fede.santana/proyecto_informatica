/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>

QT_BEGIN_NAMESPACE

class Ui_settingsdialog
{
public:
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *settingsdialog)
    {
        if (settingsdialog->objectName().isEmpty())
            settingsdialog->setObjectName(QString::fromUtf8("settingsdialog"));
        settingsdialog->resize(400, 300);
        buttonBox = new QDialogButtonBox(settingsdialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(30, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        retranslateUi(settingsdialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), settingsdialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), settingsdialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(settingsdialog);
    } // setupUi

    void retranslateUi(QDialog *settingsdialog)
    {
        settingsdialog->setWindowTitle(QCoreApplication::translate("settingsdialog", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class settingsdialog: public Ui_settingsdialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
