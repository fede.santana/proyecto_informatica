/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionConfigure;
    QWidget *centralWidget;
    QPushButton *bIzq;
    QPushButton *Centrar_en_Y;
    QPushButton *Centrar_en_X_3;
    QPushButton *bArr;
    QPushButton *bAba;
    QPushButton *bDer;
    QPushButton *Centrar_en_Y_2;
    QStatusBar *statusBar;
    QToolBar *toolBar;
    QMenuBar *menuBar;
    QMenu *menuMENU;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(436, 288);
        QPalette palette;
        MainWindow->setPalette(palette);
        MainWindow->setAcceptDrops(true);
        MainWindow->setWindowOpacity(0.000000000000000);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        actionConnect = new QAction(MainWindow);
        actionConnect->setObjectName(QString::fromUtf8("actionConnect"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/connect.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConnect->setIcon(icon);
        actionDisconnect = new QAction(MainWindow);
        actionDisconnect->setObjectName(QString::fromUtf8("actionDisconnect"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/disconnect.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDisconnect->setIcon(icon1);
        actionConfigure = new QAction(MainWindow);
        actionConfigure->setObjectName(QString::fromUtf8("actionConfigure"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/settings.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConfigure->setIcon(icon2);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setAutoFillBackground(true);
        bIzq = new QPushButton(centralWidget);
        bIzq->setObjectName(QString::fromUtf8("bIzq"));
        bIzq->setEnabled(true);
        bIzq->setGeometry(QRect(180, 60, 40, 61));
        bIzq->setMaximumSize(QSize(40, 80));
        QPalette palette1;
        bIzq->setPalette(palette1);
        bIzq->setAutoFillBackground(false);
        bIzq->setStyleSheet(QString::fromUtf8(""));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Flechita (3. copia).png"), QSize(), QIcon::Normal, QIcon::Off);
        bIzq->setIcon(icon3);
        bIzq->setIconSize(QSize(60, 60));
        bIzq->setCheckable(false);
        bIzq->setAutoExclusive(false);
        bIzq->setFlat(true);
        Centrar_en_Y = new QPushButton(centralWidget);
        Centrar_en_Y->setObjectName(QString::fromUtf8("Centrar_en_Y"));
        Centrar_en_Y->setGeometry(QRect(10, 50, 131, 41));
        QPalette palette2;
        Centrar_en_Y->setPalette(palette2);
        Centrar_en_Y->setAcceptDrops(false);
        Centrar_en_Y->setAutoFillBackground(false);
        Centrar_en_Y->setStyleSheet(QString::fromUtf8(""));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/HomeY.png"), QSize(), QIcon::Normal, QIcon::Off);
        Centrar_en_Y->setIcon(icon4);
        Centrar_en_Y->setIconSize(QSize(30, 30));
        Centrar_en_Y->setCheckable(false);
        Centrar_en_Y->setFlat(true);
        Centrar_en_X_3 = new QPushButton(centralWidget);
        Centrar_en_X_3->setObjectName(QString::fromUtf8("Centrar_en_X_3"));
        Centrar_en_X_3->setGeometry(QRect(10, 87, 131, 41));
        QPalette palette3;
        Centrar_en_X_3->setPalette(palette3);
        Centrar_en_X_3->setAcceptDrops(false);
        Centrar_en_X_3->setAutoFillBackground(false);
        Centrar_en_X_3->setStyleSheet(QString::fromUtf8(""));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/HomeX.png"), QSize(), QIcon::Normal, QIcon::Off);
        Centrar_en_X_3->setIcon(icon5);
        Centrar_en_X_3->setIconSize(QSize(30, 30));
        Centrar_en_X_3->setCheckable(false);
        Centrar_en_X_3->setFlat(true);
        bArr = new QPushButton(centralWidget);
        bArr->setObjectName(QString::fromUtf8("bArr"));
        bArr->setGeometry(QRect(200, 10, 89, 41));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/images/Flechita.png"), QSize(), QIcon::Normal, QIcon::Off);
        bArr->setIcon(icon6);
        bArr->setIconSize(QSize(60, 60));
        bArr->setFlat(true);
        bAba = new QPushButton(centralWidget);
        bAba->setObjectName(QString::fromUtf8("bAba"));
        bAba->setGeometry(QRect(210, 90, 61, 41));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/images/Flechita (copia).png"), QSize(), QIcon::Normal, QIcon::Off);
        bAba->setIcon(icon7);
        bAba->setIconSize(QSize(60, 60));
        bAba->setFlat(true);
        bDer = new QPushButton(centralWidget);
        bDer->setObjectName(QString::fromUtf8("bDer"));
        bDer->setGeometry(QRect(240, 40, 51, 61));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/images/Flechita (otra copia).png"), QSize(), QIcon::Normal, QIcon::Off);
        bDer->setIcon(icon8);
        bDer->setIconSize(QSize(60, 60));
        bDer->setFlat(true);
        Centrar_en_Y_2 = new QPushButton(centralWidget);
        Centrar_en_Y_2->setObjectName(QString::fromUtf8("Centrar_en_Y_2"));
        Centrar_en_Y_2->setGeometry(QRect(210, 50, 42, 36));
        QPalette palette4;
        Centrar_en_Y_2->setPalette(palette4);
        Centrar_en_Y_2->setAcceptDrops(false);
        Centrar_en_Y_2->setAutoFillBackground(false);
        Centrar_en_Y_2->setStyleSheet(QString::fromUtf8(""));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/images/Home.png"), QSize(), QIcon::Normal, QIcon::Off);
        Centrar_en_Y_2->setIcon(icon9);
        Centrar_en_Y_2->setIconSize(QSize(30, 30));
        Centrar_en_Y_2->setCheckable(false);
        Centrar_en_Y_2->setFlat(true);
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 436, 20));
        menuMENU = new QMenu(menuBar);
        menuMENU->setObjectName(QString::fromUtf8("menuMENU"));
        MainWindow->setMenuBar(menuBar);

        toolBar->addAction(actionConnect);
        toolBar->addAction(actionDisconnect);
        toolBar->addAction(actionConfigure);
        menuBar->addAction(menuMENU->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MOTORES EN ACCION ahre", nullptr));
        actionConnect->setText(QCoreApplication::translate("MainWindow", "Connect", nullptr));
        actionDisconnect->setText(QCoreApplication::translate("MainWindow", "Disconnect", nullptr));
        actionConfigure->setText(QCoreApplication::translate("MainWindow", "Configure", nullptr));
        bIzq->setText(QString());
        Centrar_en_Y->setText(QString());
        Centrar_en_X_3->setText(QString());
        bArr->setText(QString());
        bAba->setText(QString());
        bDer->setText(QString());
        Centrar_en_Y_2->setText(QString());
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "toolBar", nullptr));
        menuMENU->setTitle(QCoreApplication::translate("MainWindow", "MENU", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
