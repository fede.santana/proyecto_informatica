/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *speedButton;
    QLabel *label_2;
    QLineEdit *speed;
    QLineEdit *steps;
    QPushButton *stepsButton;
    QPushButton *pushButton;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 161, 51));
        speedButton = new QPushButton(centralWidget);
        speedButton->setObjectName(QStringLiteral("speedButton"));
        speedButton->setGeometry(QRect(180, 50, 111, 31));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 90, 101, 41));
        speed = new QLineEdit(centralWidget);
        speed->setObjectName(QStringLiteral("speed"));
        speed->setGeometry(QRect(10, 50, 151, 31));
        steps = new QLineEdit(centralWidget);
        steps->setObjectName(QStringLiteral("steps"));
        steps->setGeometry(QRect(10, 130, 151, 31));
        stepsButton = new QPushButton(centralWidget);
        stepsButton->setObjectName(QStringLiteral("stepsButton"));
        stepsButton->setGeometry(QRect(180, 130, 111, 31));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(120, 210, 80, 23));
        QPalette palette;
        QBrush brush(QColor(239, 62, 62, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        pushButton->setPalette(palette);
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Prueba1", nullptr));
        label->setText(QApplication::translate("MainWindow", "Ingresar velocidad (RPM)", nullptr));
        speedButton->setText(QApplication::translate("MainWindow", "Introducir", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Ingresar pasos", nullptr));
        stepsButton->setText(QApplication::translate("MainWindow", "PushMe!", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "DETENER", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
