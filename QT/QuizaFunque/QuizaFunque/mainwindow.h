#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QByteArray>
#include <QObject>
#include <QSerialPort>
#include <QTextStream>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //QSerialPort::QSerialPort();
    //QSerialPort::QSerialPort(QObject *parent = nullptr) ;
private slots:
    //void openSerialPort();
    //void closeSerialPort();
    //void about();
    //void writeData(const QByteArray &data);
    //void readData();

private:
    Ui::MainWindow *ui;
    QSerialPort m_serial;

};


#endif // MAINWINDOW_H
