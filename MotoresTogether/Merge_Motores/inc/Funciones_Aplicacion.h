

#ifndef FUNCIONES_APLICACION_H_
#define FUNCIONES_APLICACION_H_
#include <Stepper.h>

//Estados mde principal
#define GOING_HOME			0
#define READY 				1
#define MOVING				2

void youAreDrunkGoHome(fourWireStepper_t* PaP_func);
void moveRight(int qtySteps);
void moveLeft(int qtySteps);
void moveRightMax(fourWireStepper_t* PaP_func);
void moveLeftMax(fourWireStepper_t* PaP_func);

void moveUp(int grados);
void moveDown(int grados);

#endif /* FUNCIONES_APLICACION_H_ */
