
// DIRECCIONES UART0

#define		DIR_UART0		( ( __RW uint32_t  * ) 0x4000C000UL )

#define		UART0RBR		DIR_UART0[0]	// Registro de Recepción RBR
#define		UART0THR		DIR_UART0[0]	// Registro de Transmisión THR
#define		UART0DLL		DIR_UART0[0]	// Parte baja del divisor de la UART0:
#define		UART0IER			DIR_UART0[1]	// Registro de Habilitación de interrupciones de la UART0:
#define		UART0DLM		DIR_UART0[1]	// Parte Alta del divisor de la UART0:
#define		UART0IIR			DIR_UART0[2]	// Registro de Identificación de interrupciones de la UART0:
#define		UART0LCR		DIR_UART0[3]	// Line CONTROL Register de la UART0:
#define		UART0LSR		DIR_UART0[5]	// Line STATUS Register de la UART0:

//Macros UART0
#define		UART0DLAB_OFF	(U0LCR & 0xEF)
#define		UART0DLAB_ON	(U0LCR | 0x80)

#define 	TX0			PORT0, 2	//Tx de la UART0
#define 	RX0			PORT0, 3	//Rx de la UART0

#define BAUDIOS 9600
#define REPOSO 1
#define COMANDO 2
#define FIN_LECTURA 3
#define MAX_BUFF_RX		512
#define MAX_BUFF_TX		512
#define UART0	0
#define UART1	1

//0x40010000UL : Registro de recepcion de la UART1:
#define		DIR_UART1		( ( __RW uint32_t  * ) 0x40010000UL )
#define		UART1RBR		DIR_UART1[0]
#define		UART1THR		DIR_UART1[0]
#define		UART1DLL		DIR_UART1[0]
#define		UART1IER		DIR_UART1[1]
#define		UART1DLM		DIR_UART1[1]
#define		UART1IIR		DIR_UART1[2]
#define		UART1LCR		DIR_UART1[3]
#define		UART1LSR		DIR_UART1[5]

#define 	TX1				PORT0 , 15	//Tx de la UART1
#define 	RX1				PORT0 , 16	//Rx de la UART1

#define		TXD			1
#define		RXD			1

#define		DIR_UART1		( ( __RW uint32_t  * ) 0x40010000UL )

#define		UART1RBR		DIR_UART1[0]
#define		UART1THR		DIR_UART1[0]
#define		UART1DLL		DIR_UART1[0]
#define		UART1IER		DIR_UART1[1]
#define		UART1DLM		DIR_UART1[1]
#define		UART1IIR		DIR_UART1[2]
#define		UART1LCR		DIR_UART1[3]
#define		UART1LSR		DIR_UART1[5]

#define 	TX1				PORT0 , 15	//Tx de la UART1
#define 	RX1				PORT0 , 16	//Rx de la UART1

#define		TXD				1
#define		RXD				1


//DECLARACION FUNCIONES UART0
void MDE_QT (void);
void ejecutarComando (int16_t );
void UART0_Inicializacion (uint32_t );
void UART1_Inicializacion ( uint32_t );
void UART0_PushRX( uint8_t dato );
int16_t UART0_PopRX( void );
void UART0_PushTX( uint8_t dato );
int16_t UART0_PopTX( void );
int16_t Transmitir ( uint8_t com , const void * datos , uint8_t cant );
int16_t UART_PopRX( uint8_t uart);
void UART1_PushRX( uint8_t dato );
int16_t UART1_PopRX( void );
void UART1_PushTX( uint8_t dato );
int16_t UART1_PopTX( void );
