################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../inc/DR_UART0.c \
../inc/DR_UART1.c \
../inc/PR_UART.c 

OBJS += \
./inc/DR_UART0.o \
./inc/DR_UART1.o \
./inc/PR_UART.o 

C_DEPS += \
./inc/DR_UART0.d \
./inc/DR_UART1.d \
./inc/PR_UART.d 


# Each subdirectory must supply rules for building sources it contributes
inc/%.o: ../inc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M3 -D__LPC17XX__ -D__REDLIB__ -I"/home/federico/Desktop/GitHub/proyecto_informatica/MotoresTogether/Merge_Motores/inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


