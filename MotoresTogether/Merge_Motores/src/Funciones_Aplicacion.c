
#include <Funciones_Aplicacion.h>

#define RETROCESO 20

extern int numberSteps;
extern int GradosActualizadosDelQT;
extern int flagCambioAlgoElQT;
extern int qtCambioStepper;
void youAreDrunkGoHome(fourWireStepper_t* PaP_func)
{

	step(RETROCESO, PaP_func);
	return;
}

void moveRight(int qtySteps)
{
	numberSteps+=qtySteps;
	qtCambioStepper = 1;
	return;
}
void moveLeft(int qtySteps)
{
	numberSteps-=qtySteps;
	qtCambioStepper = 1;
	return;
}
void moveRightMax(fourWireStepper_t* PaP_func)
{
	numberSteps = PaP_func->maxStepsRight;
	return;
}
void moveLeftMax(fourWireStepper_t* PaP_func)
{
	numberSteps = PaP_func->maxStepsLeft;
	return;
}

void moveUp(int grados)
{
	GradosActualizadosDelQT+=grados;
	flagCambioAlgoElQT = 1;
	return;
}
void moveDown(int grados)
{
	GradosActualizadosDelQT-=grados;
	flagCambioAlgoElQT = 1;
	return;
}
