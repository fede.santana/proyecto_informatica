
#include <PWM.h>

extern int actualPlace;
extern int flagCambioAlgoElQT;
extern int GradosActualizadosDelQT;

void updateMR1(int newPlace)
{
	int aux;
	static int movementLeft=-1;
	static int direction=0;

	if(newPlace != -1)
	{
		if(movementLeft == 0) return;
		if (actualPlace > newPlace)
			{
				direction = ANTIHORARIO; //Se mueve en sentido antihorario
				movementLeft = actualPlace - newPlace; //Cantidad de grados
			}
		else
			{
				direction = HORARIO; //Se mueve en sentido horario
				movementLeft = newPlace - actualPlace;
			}
	}

	if(direction == HORARIO)
	{
		aux = actualPlace + 1; //Nos movemos un grado
		if(aux > 130) aux = 130;
		PWM1MR1 = (500 + (aux*2000/180)); // pulse width 1.5ms
		PWM1LER |=  (0x01<<1); //Apply changes
		actualPlace++;
		movementLeft--;
		if(GradosActualizadosDelQT >0) GradosActualizadosDelQT--;
		else GradosActualizadosDelQT++;
	}

	if(direction == ANTIHORARIO)
	{
			aux = actualPlace - 1; //Nos movemos un grado
			if(aux > 100) aux = 100;
			PWM1MR1 = (500 + (aux*2000/180)); // pulse width 1.5ms
			PWM1LER |=  (0x01<<1); //Apply changes
			actualPlace--;
			movementLeft--;
			if(GradosActualizadosDelQT >0) GradosActualizadosDelQT--;
			else GradosActualizadosDelQT++;
	}
}

