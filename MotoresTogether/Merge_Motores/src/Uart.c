#include <DR_tipos.h>
#include <Uart.h>
#include <Funciones_Aplicacion.h>
// DECLARACIONES  UART0

volatile static uint8_t UART0_BufferRx[MAX_BUFF_RX];
volatile static uint8_t UART0_BufferTx[MAX_BUFF_TX];
volatile static uint8_t UART0_inRX , UART0_outRX , UART0_inTX, UART0_outTX;
volatile static int16_t UART0_flagTx;
// DECLARACIONES UART1
volatile static uint8_t UART1_BufferRx[MAX_BUFF_RX];
volatile static uint8_t UART1_BufferTx[MAX_BUFF_TX];
volatile static uint8_t	UART1_inRX,UART1_outRX,UART1_inTX,UART1_outTX;
volatile static int16_t UART1_flagTx;

void UART0_PushRX( uint8_t dato )
{
	UART0_BufferRx[ UART0_inRX ] = dato;
	UART0_inRX ++;
	UART0_inRX %= MAX_BUFF_RX;
}
int16_t UART0_PopRX( void )
{
	int16_t salida = -1;

	if ( UART0_inRX != UART0_outRX )
	{
		salida = UART0_BufferRx[ UART0_outRX ] ;

		UART0_outRX ++;
		UART0_outRX %= MAX_BUFF_RX;
	}
	return salida;
}
int16_t UART0_PopTX( void )
{
	int16_t salida = -1;

	if ( UART0_inTX != UART0_outTX )
	{
		salida = UART0_BufferTx[ UART0_outTX ] ;

		UART0_outTX ++;
		UART0_outTX %= MAX_BUFF_TX;
	}
	return salida;
}
void UART0_PushTX( uint8_t dato )
{
	if ( !UART0_flagTx )
	{
		UART0_flagTx = 1;
		UART0THR = dato;
		return;
	}

	UART0_BufferTx[ UART0_inTX ] = dato;
	UART0_inTX ++;
	UART0_inTX %= MAX_BUFF_TX;
}
void UART1_PushRX( uint8_t dato )
{
	UART1_BufferRx[ UART1_inRX ] = dato;
	UART1_inRX ++;
	UART1_inRX %= MAX_BUFF_RX;
}
int16_t UART1_PopRX( void )
{
	int16_t salida = -1;

	if ( UART1_inRX != UART1_outRX )
	{
		salida = UART1_BufferRx[ UART1_outRX ] ;
		UART1_outRX ++;
		UART1_outRX %= MAX_BUFF_RX;
	}
	return salida;
}
int16_t UART1_PopTX( void )
{
	int16_t salida = -1;

	if ( UART1_inTX != UART1_outTX )
	{
		salida = UART1_BufferTx[ UART1_outTX ] ;

		UART1_outTX ++;
		UART1_outTX %= MAX_BUFF_TX;
	}
	return salida;
}
void UART1_PushTX( uint8_t dato )
{
	if ( !UART1_flagTx )
	{
		UART1_flagTx = 1;
		UART1THR = dato;
		return;
	}

	UART1_BufferTx[ UART1_inTX ] = dato;
	UART1_inTX ++;
	UART1_inTX %= MAX_BUFF_TX;
}
void ejecutarComando (int16_t comando)
{
	switch (comando)
	{
		case 'A':
			moveLeft(16);
			break;
		case 'B':
			moveRight(16);
			break;
		case 'C':
			moveDown(5);
			break;
		case 'D':
			moveUp(5);
			break;
		default:
			// Comando no valido.
			break;
	}
	return;
}
