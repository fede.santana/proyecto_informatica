/* INTERRUPCIONES DEL SISTEMA*/
#include <DR_tipos.h>
#include <gpio.h>
#include <Stepper.h>
#include <DR_lcd.h>
#include <MyTimer.h>
#include <Uart.h>
extern unsigned long volatile miliSegundo;
extern uint8_t flagLimitSwitch;
extern int flagT0;
extern int flagT1;
extern int UART0_flagTx;
void SysTick_Handler(void)
{
	EscrituraLCD( );
//	miliSegundo--;
//	miliSegundo%=6; //Proteger para que la variable solo tome valores esperados.
}

void EINT0_IRQHandler (void)
{
	EXTINT = EXTINT | (0x01<<EINT0);			//!< Limpia Flag interrupcion
	flagLimitSwitch = 1; //!< Set Flag final de carrera
}
void TIMER0_IRQHandler(void)
{
	if((T0IR == MR0))
	{
				T0IR |= MR0;
				flagT0 = 1;
	}
}
void TIMER1_IRQHandler(void)
{
	if((T1IR == MR0))
	{
				T1IR |= MR0;
				flagT1 = 1;
	}
}

void UART0_IRQHandler (void)
{
	uint8_t iir, aux;
	int16_t datoTx;

	do
	{
		//IIR es reset por HW, una vez que lo lei se resetea.
		iir = UART0IIR;

		if ( iir & 0x04 ) 							//Data ready
		{
			//Display_lcd ("LLEGO msj -UART0", 0 , 0);
			aux = UART0RBR;
			UART0_PushRX ( aux );   	//guardo en buffer e incremento índice
									//garantizo el buffer circular
		}

		if ( iir & 0x02 ) //THRE
		{
			datoTx = UART0_PopTX();
			if ( datoTx != -1 )
				UART0THR = (uint8_t) datoTx;
			else
				UART0_flagTx = 0;
		}
	}
	while( ! ( iir & 0x01 ) ); /* me fijo si cuando entre a la ISR habia otra
						     	int. pendiente de atencion: b0=1 (ocurre unicamente si dentro del mismo
								espacio temporal lleguan dos interrupciones a la vez) */
}
