#include <PLL.h>
#include <LPC1769Regs.h>
#include "DR_Timers.h"
#include "DR_SysTick.h"
#include <gpio.h>
#include <DR_lcd.h>
#include <PWM.h>
#include <MyTimer.h>
#include <Stepper.h>
#include <Uart.h>

/* SYSTICK */
//unsigned long volatile miliSegundo = 6;
unsigned long volatile time_since_init = 0;
unsigned int volatile flag = 0;
int volatile thisStep = 0;
/* SYSTICK */

extern fourWireStepper_t PaP;
extern uint8_t flagLimitSwitch;
//Servo Motor
extern int actualPlace;

void InitAll(void){
    InicializarPLL();
	InicializarSysTick();
    Stepper(PINA, PINB, PINC, PIND, MAXSTEPS, MINSTEPS, VELOCIDAD, &PaP);
    InicializarLimitSwitch();
    InicializarPWM();
    InicializarLCD();
    InicializarTimer0();
    InicializarTimer1();
    UART0_Inicializacion (BAUDIOS);
    UART1_Inicializacion (BAUDIOS);
}

void InicializarPLL(void)
{
	SCS       = SCS_Value;

	if (SCS_Value & (1 << 5))               /* If Main Oscillator is enabled      */
		while ((SCS & (1<<6)) == 0);/* Wait for Oscillator to be ready    */

	CCLKCFG   = CCLKCFG_Value;      /* Setup Clock Divider                */

	PCLKSEL0  = PCLKSEL0_Value;     /* Peripheral Clock Selection         */
	PCLKSEL1  = PCLKSEL1_Value;

	CLKSRCSEL = CLKSRCSEL_Value;    /* Select Clock Source for PLL0       */

	PLL0CFG   = PLL0CFG_Value;      /* configure PLL0                     */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	PLL0CON   = 0x01;             /* PLL0 Enable                        */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & (1<<26)));/* Wait for PLOCK0                    */

	PLL0CON   = 0x03;             /* PLL0 Enable & Connect              */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & ((1<<25) | (1<<24))));/* Wait for PLLC0_STAT & PLLE0_STAT */

	PLL1CFG   = PLL1CFG_Value;
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	PLL1CON   = 0x01;             /* PLL1 Enable                        */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & (1<<10)));/* Wait for PLOCK1                    */

	PLL1CON   = 0x03;             /* PLL1 Enable & Connect              */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & ((1<< 9) | (1<< 8))));/* Wait for PLLC1_STAT & PLLE1_STAT */

	PCONP     = PCONP_Value;        /* Power Control for Peripherals      */

	CLKOUTCFG = CLKOUTCFG_Value;    /* Clock Output Configuration         */

	FLASHCFG  = (FLASHCFG & ~0x0000F000) | FLASHCFG_Value;
}

void InicializarSysTick ( void )
{
	STRELOAD = ( STCALIB / 4) - 1;
	STCURR = 0;

	CLKSOURCE = 1;
	ENABLE = 1;
	TICKINT = 1;
}

int Stepper(int coilPinA, int coilPinB, int coilPinC, int coilPinD, int maxStepsRight,int maxStepsLeft, int whatSpeed, fourWireStepper_t *PaP)
{

    // Inicialización variables

    PaP->currentStep = maxStepsRight + 50;
    PaP->stepTimeStamp = 0;
    PaP->maxStepsRight = maxStepsRight;
    PaP->maxStepsLeft = maxStepsLeft;
    PaP->stepDelay = 60L * 1000L / 200 / whatSpeed;

    // Traduce el numero de Pin de Expansion a su correspondiente puerto y N° de Pin

    if(decodePin(coilPinA,PaP,'A')==-1) {
        return -1;
    }
    if(decodePin(coilPinB,PaP,'B')==-1) {
        return -1;
    }
    if(decodePin(coilPinC,PaP,'C')==-1) {
        return -1;
    }
    if(decodePin(coilPinD,PaP,'D')==-1) {
        return -1;
    }

    setCoilPin(PaP); // Se configuran los pines

    return 0;
}

void InicializarLimitSwitch(void){
	SetPINSEL(2,10,PINSEL_FUNC1);
	EXTMODE |= (0x0F << 0); //Todas por flanco
	EXTPOLAR &= ~(0x0F << 0);//Todas por flanco descendiente
	ISER0 |= (0x01 << 18); //Set Enable Eint1 (Entrada digital2)
}

int decodePin(int expPin, fourWireStepper_t *PaP,char Pin)
{
    //Traduce el numero de Pin de Expansion a su correspondiente puerto y N° de Pin
    int vector[2];
    switch (expPin)
    {
    case 16:
        vector[0] = 2;
        vector[1] = 8;
        break;
    case 15:
        vector[0] = 1;
        vector[1] = 18;
        break;
    case 12:
        vector[0] = 1;
        vector[1] = 27;
        break;
    case 14:
        vector[0] = 1;
        vector[1] = 21;
        break;
    case 13:
        vector[0] = 1;
        vector[1] = 24;
        break;
    case 11:
        vector[0] = 3;
        vector[1] = 25;
        break;
    default:
        return -1;
        break;
    }
    switch(Pin){
    case 'A':
    		PaP->coilPinA[0] = vector[0];
    		PaP->coilPinA[1] = vector[1];
    		break;
    case 'B':
        	PaP->coilPinB[0] = vector[0];
        	PaP->coilPinB[1] = vector[1];
        	break;
    case 'C':
        	PaP->coilPinC[0] = vector[0];
        	PaP->coilPinC[1] = vector[1];
        	break;
    case 'D':
        	PaP->coilPinD[0] = vector[0];
        	PaP->coilPinD[1] = vector[1];
        	break;
    }
    return 0;
}

void setCoilPin(fourWireStepper_t *PaP){
    /*
    * SetPINSEL: Configura la función del PIN.
    * SetDIR: Soy OUTPUT o INPUT.
    * SetMODE_OD: Sólo cuando lo configure como salida, ¿qué tipo de salida? PUSH_PULL u OPEN_DRAIN
    */
    //Configuracion del Pin A
    SetPINSEL(PaP->coilPinA[0], PaP->coilPinA[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinA[0], PaP->coilPinA[1], OUTPUT);
    SetMODE_OD(PaP->coilPinA[0], PaP->coilPinA[1], PUSH_PULL);
    //Configuracion del Pin B
    SetPINSEL(PaP->coilPinB[0], PaP->coilPinB[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinB[0], PaP->coilPinB[1], OUTPUT);
    SetMODE_OD(PaP->coilPinB[0], PaP->coilPinB[1], PUSH_PULL);
    //Configuracion del Pin C
    SetPINSEL(PaP->coilPinC[0], PaP->coilPinC[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinC[0], PaP->coilPinC[1], OUTPUT);
    SetMODE_OD(PaP->coilPinC[0], PaP->coilPinC[1], PUSH_PULL);
    //Configuracion del Pin D
    SetPINSEL(PaP->coilPinD[0], PaP->coilPinD[1], PINSEL_GPIO);
    SetDIR(PaP->coilPinD[0], PaP->coilPinD[1], OUTPUT);
    SetMODE_OD(PaP->coilPinD[0], PaP->coilPinD[1], PUSH_PULL);

    return;
}
void InicializarPWM(void)
{
		SetPINSEL(1,18,2); //activo PWM1.1
		PCONP |= (1<<6);

		PCLKSEL0 &= ~((0x03)<<12); // 0x03 es 11 pero estoy diciendo 00.
		PCLKSEL0 |= 1 << 12;

		PWM1PCR = 0x00;		// le digo a todos single edge mode

		PWM1PR = TIMEPR;	// que se incremente a un 1ms.
		// PR = ( PCLK * PWM res ) - 1 = (100 * 1 us ) -1

		PWM1MR0 = TIMEMRO;		//20000us = 20ms period duration
		PWM1MR1 = 500;			// lo manda a la posicion 0
		PWM1MCR |= (0x01<<1);	// que se resetee cuando matchee

		PWM1LER |= (0x01<<0);	//update MR0
		PWM1LER |= (0x01<<1);	// update MR1

		PWM1PCR |= (0x01<<9);	//habilitamos output de pwm

		PWM1TCR |= (0x01<<1); //Reset Timer

		PWM1TCR = (1<<0) | (1<<3);

}
void InicializarTimer0(void)
{

	PCONP		|= (1<<1);
	PCLKSEL0 	|= (1<<2);
	T0PR 		= PR;
	T0MR0 		= 2000000;
	T0CTCR 		&= TIMER;
	T0MCR 		&= CLR_MTCH_CONFIG;
	T0MCR 		|= (1<<0);
	T0MCR 		|= (1<<1);
	T0TCR 		&= CLEAR_RST_EN;
	T0TCR 		|= (1<<1);
	T0TCR 		&= TIMER_RST_OFF;
	T0TCR 		|= (1<<0);
	ISER0 		|= (1<<1);
}

void InicializarTimer1(void)
{

	PCONP		|= (1<<2);
	PCLKSEL0 	|= (1<<4);
	T1PR 		= PR;
	T1MR0		= 1000000;
	T1CTCR 		&= TIMER;
	T1MCR 		&= CLR_MTCH_CONFIG;
	T1MCR 		|= (1<<0);
	T1MCR 		|= (1<<1);
	T1TCR 		&= CLEAR_RST_EN;
	T1TCR 		|= (1<<1);
	T1TCR 		&= TIMER_RST_OFF;
	T1TCR 		|= (1<<0);
	ISER0 		|= (1<<2);
}

void UART0_Inicializacion ( uint32_t baudios )
{
	//!< 1.- Registro PCONP - bit 3 en 1 prende la UART:
	PCONP |= 0x01<<3;

	//!< 2.- Registro PCLKSEL0 - bits 6 y 7 en 0 seleccionan que el clk de la UART0 sea CCLK/4:
	PCLKSEL0 &= ~(0x03<<6);			//!< con un CCLOK=100Mhz, nos queda PCLOCK=25Mhz

	//!< 3.- Registro U0LCR - transmision de 8 bits, 1 bit de stop, sin paridad, sin break cond, DLAB = 1:
	UART0LCR = 0x00000083;

	//!< 4.- Registros U10DLL y U0DLM
	baudios =  25000000 / ( 16 * baudios );
	UART0DLM = (baudios >> 8 ) & 0x000000ff;
	UART0DLL = baudios & 0x000000ff;

	//!< 5.- Registros PINSEL0 y PINSEL1 - habilitan las funciones especiales de los pines:
	//!< TX0 : P0[2]	-> PINSEL0: 04:05
	SetPINSEL( TX0 , TXD );
	//!< RX0 : P0[3]	-> PINSEL0: 06:07
	SetPINSEL( RX0 , RXD );

	//!< 6.- Registro U0LCR, pongo DLAB en 0:
	UART0LCR = 0x00000003;

	//!< 7. Habilito las interrupciones (En la UART -IER- y en el NVIC -ISER)
	UART0IER = 0x00000003;
	ISER0 |= ( 1 << 5);
}

void UART1_Inicializacion ( uint32_t baudios )
{
	//!< 1.- Registro PCONP - bit 3 en 1 prende la UART:
	PCONP |= 0x01<<4;

	//!< 2.- Registro PCLKSEL0 - bits 6 y 7 en 0 seleccionan que el clk de la UART0 sea CCLK/4:
	PCLKSEL1 &= ~(0x03<<6);			//!< con un CCLOK=100Mhz, nos queda PCLOCK=25Mhz

	//!< 3.- Registro U0LCR - transmision de 8 bits, 1 bit de stop, sin paridad, sin break cond, DLAB = 1:
	UART1LCR = 0x00000083;

	//!< 4.- Registros U10DLL y U0DLM
	baudios =  25000000 / ( 16 * baudios );
	UART1DLM = (baudios >> 8 ) & 0x000000ff;
	UART1DLL = baudios & 0x000000ff;

	//!< 5.- Registros PINSEL0 y PINSEL1 - habilitan las funciones especiales de los pines:
	//!< TX1 : P0[15]	-> PINSEL0: 30:31
	SetPINSEL( TX1 , TXD );
	//!< RX0 : P0[16]	-> PINSEL0: 01:00
	SetPINSEL( RX1 , RXD );

	//!< 6.- Registro U0LCR, pongo DLAB en 0:
	UART1LCR = 0x00000003;

	//!< 7. Habilito las interrupciones (En la UART -IER- y en el NVIC -ISER)
	UART1IER = 0x00000003;
	ISER0 |= ( 1 << 6);
}

