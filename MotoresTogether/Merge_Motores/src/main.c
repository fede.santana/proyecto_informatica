

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <Stepper.h>
#include <DR_Timers.h>
#include <PLL.h>
#include <DR_lcd.h>
#include <PR_lcd.h>

//Variables Stepper
volatile int flagT1;
volatile fourWireStepper_t PaP; //Fede Agrego Volatile.
volatile uint8_t flagLimitSwitch=0;
volatile int numberSteps=0;
volatile int qtCambioStepper = 0;
//Variables Servo
volatile int flagT0;
volatile int GradosActualizadosDelQT = 100;
volatile int flagCambioAlgoElQT = 0;
volatile int actualPlace;
//Variables Uart
volatile int UART0_flagTx;
//unsigned long volatile miliSegundo;
int main (void)
{

	InitAll();

    while (1)
    {
    	MDE_QT();
    	MDE_Stepper();
    	MDE_Servo();
    }

    return 0;
}
