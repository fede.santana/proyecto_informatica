#include <Stepper.h>
#include <gpio.h>
#include <LPC1769Regs.h>
#include <Funciones_Aplicacion.h>


extern int numberSteps;
extern fourWireStepper_t PaP;
extern uint8_t flagLimitSwitch;
extern int flagT1;

void offCoils (fourWireStepper_t *PaP)
{
	SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], LOW);
	SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], LOW);
	SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], LOW);
	SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], LOW);
}


void step(int steps_to_move, fourWireStepper_t *PaP)
{
    static unsigned int steps_left=0;
    if(steps_to_move!=0){
    steps_left = abs(steps_to_move); // how many steps to take


    // determine direction based on whether steps_to_mode is + or -:
    if (steps_to_move >= 0)
    {
        PaP->direction = 1;
    }
    if (steps_to_move < 0)
    {
        PaP->direction = 0;
    }
    }
    // decrement the number of steps, moving one step each time:
    if (steps_left > 0)
    { 

//    	extern unsigned long volatile miliSegundo; // Tiempo actual en microsegundos, función de Arduino.
        // move only if the appropriate delay has passed:
        if(flagT1 == 1)
        {
        	//miliSegundo = 6;
            flagT1 = 0;
            // increment or decrement the step number,
            // depending on direction:
            if (PaP->direction == 1)
            {
            	PaP->currentStep++;
                if (PaP->currentStep >= PaP->maxStepsRight)
                {
                    PaP->currentStep = PaP->maxStepsRight;
                    steps_left = 0;
                    numberSteps = 0;
                    return;
                }
            }

            else
            {
                PaP->currentStep--;
                if (PaP->currentStep <= PaP->maxStepsLeft)
                {
                    PaP->currentStep = PaP->maxStepsLeft;
                    steps_left = 0;
                    numberSteps = 0;
                    return;
                }

            }
            // decrement the steps left:
            steps_left--;
            if(numberSteps>0) numberSteps--;
            else numberSteps++;
            if(steps_left < 0) steps_left = 0;
            // step the motor to step number 0, 1, ..., {3 or 10}
            stepMotor(PaP->currentStep % 4, PaP);
        }
    }

}

void stepMotor(int thisStep, fourWireStepper_t *PaP)
{
    switch (thisStep)
    {
		case 0: // 1010
			SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], HIGH);
			SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], LOW);
			SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], HIGH);
			SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], LOW);
			break;
		case 1: // 0110
			SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], LOW);
			SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], HIGH);
			SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], HIGH);
			SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], LOW);
			break;
		case 2: //0101
			SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], LOW);
			SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], HIGH);
			SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], LOW);
			SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], HIGH);
			break;
		case 3: //1001
			SetPIN(PaP->coilPinA[0],PaP->coilPinA[1], HIGH);
			SetPIN(PaP->coilPinB[0],PaP->coilPinB[1], LOW);
			SetPIN(PaP->coilPinC[0],PaP->coilPinC[1], LOW);
			SetPIN(PaP->coilPinD[0],PaP->coilPinD[1], HIGH);
			break;
    }
}




