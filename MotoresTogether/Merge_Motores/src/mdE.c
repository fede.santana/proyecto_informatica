#include <Stepper.h>
#include <gpio.h>
#include <LPC1769Regs.h>
#include <Funciones_Aplicacion.h>
#include <PWM.h>
#include <Uart.h>
//Variables Servo
extern int flagT0;
extern int GradosActualizadosDelQT;
extern int flagCambioAlgoElQT;
extern int actualPlace;
extern int qtCambioStepper;
//Variables Stepper
extern fourWireStepper_t PaP;
extern uint8_t flagLimitSwitch;

extern int numberSteps;

void MDE_Stepper(){

	static volatile int estadoStepper = GOING_HOME;

	if(flagLimitSwitch){
		PaP.maxStepsRight = MAXSTEPSRIGHT;
		PaP.maxStepsLeft = MAXSTEPSLEFT;
		PaP.currentStep = 0;
		step(25,&PaP);
		numberSteps = 25;
		flagLimitSwitch=0;
		estadoStepper = MOVING;
	}

	switch(estadoStepper){
		case GOING_HOME:
				youAreDrunkGoHome(&PaP);
				if(PaP.currentStep == PaP.maxStepsRight) offCoils(&PaP);
				break;
		case READY:
				offCoils(&PaP);
				if(numberSteps){
					step(numberSteps,&PaP);
					estadoStepper = MOVING;
				}
				break;
		case MOVING:
				if(qtCambioStepper){
				step(numberSteps,&PaP);
				qtCambioStepper = 0;
				}
				if(!numberSteps){
					offCoils(&PaP);
					estadoStepper = READY;
				}
				if(numberSteps){
					step(0,&PaP);
				}
				break;
		default:
				estadoStepper = GOING_HOME;
				break;
		}
}
void MDE_Servo()
{
		static int estadoServo = READY;
    	switch (estadoServo)
    	{
    		case MOVING:
    			if(flagCambioAlgoElQT == 1)
    			{
       				updateMR1(actualPlace + GradosActualizadosDelQT);
    		  		flagCambioAlgoElQT = 0;
     			}

    			if(GradosActualizadosDelQT != 0)
    			{
    				if(flagT0 == 1)
    				{
    					updateMR1(-1);
    					flagT0 = 0;
    				}
    			}
    			else
    				estadoServo = READY;
    		break;

    		case READY:
    			if(GradosActualizadosDelQT != 0)//Llegan grados
    			{
    				updateMR1(GradosActualizadosDelQT); //Escribo todos los grados
    				estadoServo = MOVING;	//Mando a grados
    			}
    		break;
    	}
}

void MDE_QT (void)
{
	static int16_t comando; // Guardará el comando.
	static uint8_t estado = REPOSO; // Solo habran los estados 1,2 ,3 por tanto uint8_t es suficiente.

	int16_t dato, datoQT, datoServer;
	datoQT = UART0_PopRX(); // Recibo un dato de la UART.
	datoServer = UART1_PopRX();

	if (datoServer != -1)
	{
		dato = datoServer;
	}

	else if (datoQT != -1)
	{
		dato = datoQT; // El QT siempre prevalecerá respecto el servidor.
	}

	else
	{
		dato = -1;
	}

	if(dato != -1)
	{
		switch (estado) // Maquina de estados para recibir la trama.
		{
			case REPOSO:
				if (dato == '#')
				{
					estado = COMANDO;
				}
				break;

			case COMANDO:
				if (dato == 'A' || dato == 'B' || dato == 'C' || dato == 'D')
				{
					estado = FIN_LECTURA;
				}
				else
				{
					estado = REPOSO;
				}

				comando = dato;
				break;

			case FIN_LECTURA:
				if (dato == '$')
				{
					ejecutarComando(comando);
					estado = REPOSO;
				}
				else
				{
					estado = REPOSO;
				}
				break;

			default: // Cadena inválida o fragmentada:
				estado = REPOSO;
				break;
		}
	}
	return;
}


