/* 
 * Stepper Motor Control - one revolution
 * Este programa impulsa un motor paso a paso unipolar o bipolar. 
 * El motor está conectado a los pines digitales 8 - 11 de la Arduino. 
 * El motor debe girar una vuelta en una dirección, a continuación, 
 * una revolución en la otra dirección. 
 * Created 11 Mar. 2007
 * by Tom Igoe
 *
 * Modificado
 * 16/05/14
 * por Andres Cruz
 * ELECTRONILAB.CO
 *
 * Modificado
 * 15/08/19
 * por F. Santana, A. Cabrera, F. Gianmuso, F. Purpura.
 */

#include <Stepper.h>

const int stepsPerRevolution = 200; // cambie este valor por el numero de pasos de su motor

#define MYSPEED 60
// inicializa la libreria 'stepper' en los pines 8 a 11
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

void setup()
{
    // establece la velocidad en 60rpm
    myStepper.setSpeed(MYSPEED);
    // inicializa el puerto serial
    Serial.begin(9600);
}

void loop()
{
    // gira una vuelta en una direccion
    Serial.println("Una vueltita para la derecha.");
    myStepper.step(stepsPerRevolution);
    delay(2000);

    // gira otra vuelta en la otra direccion
    Serial.println("Una vueltita para la izquierda.");
    myStepper.step(-stepsPerRevolution);
    delay(2000);
}