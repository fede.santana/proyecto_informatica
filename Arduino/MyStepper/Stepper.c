#include "Stepper.h"



int Stepper(int coilPinA, int coilPinB, int coilPinC, int coilPinD, int maxSteps, int whatSpeed, fourWireStepper_t* PaP)
{

    PaP->currentStep = 0;
    PaP->stepTimeStamp = 0;

    PaP->coilPinA = coilPinA;
    PaP->coilPinB = coilPinB;
    PaP->coilPinC = coilPinC;
    PaP->coilPinD = coilPinD;

    PaP->maxSteps = maxSteps;

    PaP->stepDelay = 60L * 1000L * 1000L / maxSteps / whatSpeed;

    /* CODIGO PARA ARDUINO */
    /* Se deberá declarar de forma similar para nuestra placa. */
    pinMode(PaP->coilPinA, OUTPUT);
    pinMode(PaP->coilPinB, OUTPUT);
    pinMode(PaP->coilPinC, OUTPUT);
    pinMode(PaP->coilPinD, OUTPUT);
}

void step(int steps_to_move, fourWireStepper_t* PaP)
{
    int steps_left = abs(steps_to_move); // how many steps to take

    // determine direction based on whether steps_to_mode is + or -:
    if (steps_to_move >= 0)
    {
        PaP->direction = 1;
    }
    if (steps_to_move < 0)
    {
        PaP->direction = 0;
    }

    // decrement the number of steps, moving one step each time:
    while (steps_left > 0)
    {
        unsigned long now = micros(); // Tiempo actual en microsegundos, función de Arduino.
        // move only if the appropriate delay has passed:
        if (now - PaP->stepTimeStamp >= PaP->stepDelay)
        {
            PaP->stepTimeStamp = now;
            // increment or decrement the step number,
            // depending on direction:
            if (PaP->direction == 1)
            {
                PaP->currentStep++;
                if (PaP->currentStep == PaP->maxSteps)
                {
                    PaP->currentStep = 0;
                }
            }

            else
            {
                if (PaP->currentStep == 0)
                {
                    PaP->currentStep = PaP->maxSteps;
                }
                PaP->currentStep--;
            }
            // decrement the steps left:
            steps_left--;
            // step the motor to step number 0, 1, ..., {3 or 10}
            stepMotor(PaP->currentStep % 4, PaP);
        }
    }
}

void stepMotor(int thisStep, fourWireStepper_t* PaP)
{
    switch (thisStep)
    {
    case 0: // 1010
        digitalWrite(PaP->coilPinA, HIGH);
        digitalWrite(PaP->coilPinB, LOW);
        digitalWrite(PaP->coilPinC, HIGH);
        digitalWrite(PaP->coilPinD, LOW);
        break;
    case 1: // 0110
        digitalWrite(PaP->coilPinA, LOW);
        digitalWrite(PaP->coilPinB, HIGH);
        digitalWrite(PaP->coilPinC, HIGH);
        digitalWrite(PaP->coilPinD, LOW);
        break;
    case 2: //0101
        digitalWrite(PaP->coilPinA, LOW);
        digitalWrite(PaP->coilPinB, HIGH);
        digitalWrite(PaP->coilPinC, LOW);
        digitalWrite(PaP->coilPinD, HIGH);
        break;
    case 3: //1001
        digitalWrite(PaP->coilPinA, HIGH);
        digitalWrite(PaP->coilPinB, LOW);
        digitalWrite(PaP->coilPinC, LOW);
        digitalWrite(PaP->coilPinD, HIGH);
        break;
    }
}