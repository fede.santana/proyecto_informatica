#define TIME 500
#define coil_0  0
#define coil_1  1
#define coil_2  2
#define coil_3  3
int estado=coil_0;

void setup() {
  // put your setup code here, to run once:
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  
  digitalWrite(0, HIGH);
  digitalWrite(1, LOW);
  digitalWrite(2, LOW);
  digitalWrite(3, LOW);
}

void loop() {
  
  // put your main code here, to run repeatedly:
moveRight(2);
moveLeft(3);

}
void moveRight(int qtySteps){
  int w=0;
  while(w<qtySteps){
    w++;
    stepperMotorRight();
  }
}
void moveLeft(int qtySteps){
  int i=0;
  while(i<qtySteps){
    i++;
    stepperMotorLeft();
  }
}
void stepperMotorRight(){
  if(estado==coil_3)estado=coil_0;
  else estado++;
  moveMotor(estado);
  delay(TIME);
}
void stepperMotorLeft(){
  if(estado==coil_0)estado=coil_3;
  else estado--;
  moveMotor(estado);
  delay(TIME);
}
void moveMotor(int esta2){
switch(esta2){
  case coil_0:
              digitalWrite(0,HIGH);
              digitalWrite(1,LOW);
              digitalWrite(2,LOW);
              digitalWrite(3,LOW);
              break;
  case coil_1:
              digitalWrite(0,LOW);
              digitalWrite(1,HIGH);
              digitalWrite(2,LOW);
              digitalWrite(3,LOW);
              break;
  case coil_2:
              digitalWrite(0,LOW);
              digitalWrite(1,LOW);
              digitalWrite(2,HIGH);
              digitalWrite(3,LOW);
              break;
  case coil_3:
              digitalWrite(0,LOW);
              digitalWrite(1,LOW);
              digitalWrite(2,LOW);
              digitalWrite(3,HIGH);
              break;
}
}
