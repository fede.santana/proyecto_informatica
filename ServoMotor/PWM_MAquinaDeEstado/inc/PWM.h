

#include <PLL.h>
#include <LPC1769Regs.h>
#include <gpio.h>
#include <MyTimer.h>

// Declaración de funciones

void 	initPLL();	//configuramos los timers para CCLK = 100Mhz and PCLK = 25Mhz
void	initPinPWM();	// configuramos los pines en PWM
void	initAll();
void	updateMR1(int);
void 	initPWM();

//Declaración de macros de direcciones de memoria

#define 	PWM1	(( registro_t *) 0x40018000 )
#define		PWM1IR 	PWM1[0]
#define		PWM1TCR	PWM1[1]
#define		PWM1TC	PWM1[2]
#define		PWM1PR	PWM1[3]
#define		PWM1PC  PWM1[4]
#define		PWM1MCR	PWM1[5]
#define		PWM1MR0	PWM1[6]
#define		PWM1MR1	PWM1[7]
#define		PWM1MR2	PWM1[8]
#define		PWM1MR3	PWM1[9]
#define 	PWM1CCR	PWM1[10]
#define 	PWM1CR0	PWM1[11]
#define 	PWM1CR1	PWM1[12]
#define 	PWM1CR2	PWM1[13]
#define		PWM1CR3	PWM1[14]
#define 	PWM1MR4	(( registro_t*) 0x40018040)
#define		PWM1MR5	PWM1MR4[1]
#define		PWM1MR6	PWM1MR4[2]
#define		PWM1PCR	PWM1MR4[3]
#define		PWM1LER	PWM1MR4[4]
#define		PWM1CTCR (( registro_t*) 0x40018070)

//Declaracion de constantes
#define 	TIMEMRO		20000
#define		TIMEPR		100-1
#define		INITPLACE	0
#define		HORARIO		1
#define		ANTIHORARIO	-1
#define		READY		1
#define		MOVING		2
