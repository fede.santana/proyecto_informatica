#include <LPC1769Regs.h>
#include <PLL.h>
#include <gpio.h>

//Declarion funciones

void InitTimer0(void);

// Direcciones de memoria Timer 0

#define		TIMER0 		( ( __RW uint32_t  * ) 0x40004000UL )
#define		T0IR		TIMER0[0]
#define		T0TCR		TIMER0[1]
#define		T0TC		TIMER0[2]
#define		T0PR		TIMER0[3]
#define		T0PC		TIMER0[4]
#define		T0MCR		TIMER0[5]
#define		T0MR0		TIMER0[6]
#define		T0MR1		TIMER0[7]
#define		T0MR2		TIMER0[8]
#define		T0MR3		TIMER0[9]
#define		T0CCR		TIMER0[10]
#define		T0CR0		TIMER0[11]
#define		T0CR1		TIMER0[12]
#define		_T0EMR	( ( __RW uint32_t  * ) 0x4000403CUL )
#define		T0EMR	_T0EMR[0]
#define		_T0CTCR	( ( __RW uint32_t  * ) 0x40004070UL )
#define		T0CTCR	_T0CTCR[0]

// DECLARACION CTES
#define CLR_MTCH_CONFIG		0xFFFFF000
#define CLEAR_RST_EN		0xFFFFFFFC
#define TIMER_RST_OFF		0xFFFFFFFD
#define PR					0x00
#define TIMER_RST_OFF		0xFFFFFFFD
#define MR0					0x01
#define TIMER				0xFFFFFFFC
