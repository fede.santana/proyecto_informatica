/*
===============================================================================
 Name        : PWM_GENUINO_CASI.c
 Author      : Ari
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/


#include <PWM.h>

static int actualPlace;
extern int flagCambioAlgoElQT;
extern int GradosActualizadosDelQT;

void initAll()
{
	initPLL();
	initPinPWM();
	initPWM();
	InitTimer0();
}

void initPLL(void)
{
	SCS       = SCS_Value;

	if (SCS_Value & (1 << 5))               /* If Main Oscillator is enabled      */
		while ((SCS & (1<<6)) == 0);/* Wait for Oscillator to be ready    */

	CCLKCFG   = CCLKCFG_Value;      /* Setup Clock Divider                */

	PCLKSEL0  = PCLKSEL0_Value;     /* Peripheral Clock Selection         */
	PCLKSEL1  = PCLKSEL1_Value;

	CLKSRCSEL = CLKSRCSEL_Value;    /* Select Clock Source for PLL0       */

	PLL0CFG   = PLL0CFG_Value;      /* configure PLL0                     */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	PLL0CON   = 0x01;             /* PLL0 Enable                        */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & (1<<26)));/* Wait for PLOCK0                    */

	PLL0CON   = 0x03;             /* PLL0 Enable & Connect              */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & ((1<<25) | (1<<24))));/* Wait for PLLC0_STAT & PLLE0_STAT */

	PLL1CFG   = PLL1CFG_Value;
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	PLL1CON   = 0x01;             /* PLL1 Enable                        */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & (1<<10)));/* Wait for PLOCK1                    */

	PLL1CON   = 0x03;             /* PLL1 Enable & Connect              */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & ((1<< 9) | (1<< 8))));/* Wait for PLLC1_STAT & PLLE1_STAT */

	PCONP     = PCONP_Value;        /* Power Control for Peripherals      */

	CLKOUTCFG = CLKOUTCFG_Value;    /* Clock Output Configuration         */

	FLASHCFG  = (FLASHCFG & ~0x0000F000) | FLASHCFG_Value;
}
void initPinPWM(void)
{
	SetPINSEL(1,18,2); //activo PWM1.1
}
void initPWM(void)
{
		PCONP |= (1<<6);

		PCLKSEL0 &= ~((0x03)<<12); // 0x03 es 11 pero estoy diciendo 00.
		PCLKSEL0 |= 1 << 12;

		PWM1PCR = 0x00;		// le digo a todos single edge mode

		PWM1PR = TIMEPR;	// que se incremente a un 1ms.
		// PR = ( PCLK * PWM res ) - 1 = (100 * 1 us ) -1

		PWM1MR0 = TIMEMRO;		//20000us = 20ms period duration
		PWM1MR1 = 1000;			// lo manda a la posicion 0
		actualPlace = INITPLACE;
		PWM1MCR |= (0x01<<1);	// que se resetee cuando matchee

		PWM1LER |= (0x01<<0);	//update MR0
		PWM1LER |= (0x01<<1);	// update MR1

		PWM1PCR |= (0x01<<9);	//habilitamos output de pwm

		PWM1TCR |= (0x01<<1); //Reset Timer

		PWM1TCR = (1<<0) | (1<<3);
}

void updateMR1(int newPlace)
{
	int aux;
	static int movementLeft=-1;
	static int direction=0;

	if(newPlace != -1)
	{
		if(movementLeft == 0) return;
		if (actualPlace > newPlace)
			{
				direction = ANTIHORARIO; //Se mueve en sentido antihorario
				movementLeft = actualPlace - newPlace; //Cantidad de grados
			}
		else
			{
				direction = HORARIO; //Se mueve en sentido horario
				movementLeft = newPlace - actualPlace;
			}
	}

	if(direction == HORARIO)
	{
		aux = actualPlace + 1; //Nos movemos un grado
		PWM1MR1 = (1000 + (aux*1000/180)); // pulse width 1.5ms
		PWM1LER |=  (0x01<<1); //Apply changes
		actualPlace++;
		movementLeft--;
		if(GradosActualizadosDelQT >0) GradosActualizadosDelQT--;
		else GradosActualizadosDelQT++;
	}

	if(direction == ANTIHORARIO)
	{
			aux = actualPlace - 1; //Nos movemos un grado
			PWM1MR1 = (1000 + (aux*1000/180)); // pulse width 1.5ms
			PWM1LER |=  (0x01<<1); //Apply changes
			actualPlace--;
			movementLeft--;
			if(GradosActualizadosDelQT >0) GradosActualizadosDelQT--;
			else GradosActualizadosDelQT++;
	}
}
