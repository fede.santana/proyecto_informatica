

#include <PWM.h>

volatile int flagT0;
volatile int GradosActualizadosDelQT = 180;
volatile int flagCambioAlgoElQT = 0;

int main()
{
    initAll();

    while (1)
    {
    	static int estadoServo = READY;
    	switch (estadoServo)
    	{
    		case MOVING:
    			if(flagCambioAlgoElQT == 1)
    			{
       				updateMR1(GradosActualizadosDelQT);
    		  		flagCambioAlgoElQT = 0;
     			}

    			if(GradosActualizadosDelQT != 0)
    			{
    				if(flagT0 == 1)
    				{
    					updateMR1(-1);
    					flagT0 = 0;
    				}
    			}
    			else
    				estadoServo = READY;
    		break;

    		case READY:
    			if(GradosActualizadosDelQT != 0)//Llegan grados
    			{
    				updateMR1(GradosActualizadosDelQT); //Escribo todos los grados
    				estadoServo = MOVING;	//Mando a grados
    			}
    		break;
    	}
    }
}
