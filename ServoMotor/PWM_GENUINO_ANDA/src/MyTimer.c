#include <MyTimer.h>

int flag;

void InitTimer0(void)
{
	PCONP |= (1<<1);	//activamos PCTIM0
	PCLKSEL |=(0x01<<2);
	T0PR = TIMEPR;
	T0TCR |= (1<<0);
	T0MR0 = 20000;	//20ms
	T0CTCR |= (0x00);
	T0MCR |=(0x01);
	T0MCR |=(0x01<<1);
	ISER0 |=(1<<1);
}

int TIMER0_IRQHANDLER (void)
{
	if (T0IR == HIGH)
	{
		TOIR |= HIGH;
		flag = 1;
	}
}

int main ()
{
	while(1)
	{
		if (flag)
		{
			SetPIN(2,1,1);
		}
		else
		{
			SetPin(2,1,0);
		}
	}
}
