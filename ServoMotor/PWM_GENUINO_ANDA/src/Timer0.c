#include <MyTimer.h>

extern int flagT0;

void InitTimer0(void)
{

	PCONP		|= (1<<1);
	PCLKSEL0 	|= (1<<2);
	T0PR 		= PR;
	T0MR0 		= 2000000;
	T0CTCR 		&= TIMER;
	T0MCR 		&= CLR_MTCH_CONFIG;
	T0MCR 		|= (1<<0);
	T0MCR 		|= (1<<1);
	T0TCR 		&= CLEAR_RST_EN;
	T0TCR 		|= (1<<1);
	T0TCR 		&= TIMER_RST_OFF;
	T0TCR 		|= (1<<0);
	ISER0 		|= (1<<1);
}

void TIMER0_IRQHandler(void)
{
		if((T0IR == MR0))
			{
				T0IR |= MR0;
				flagT0 = 1;
			}
}
