/*
===============================================================================
 Name        : PWM_GENUINO_CASI.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <PLL.h>
#include <LPC1769Regs.h>
#include <gpio.h>
// TODO: insert other include files here
static int i = 0;
// TODO: insert other definitions and declarations here
void initTimer0();
int main(void) {

    // TODO: insert code here

	initTimer0();
	/*Assuming that PLL0 has been setup with CCLK = 100Mhz and PCLK = 25Mhz.*/

	PCONP |= (1<<6);

	PCLKSEL0 &= ~((0x03)<<12); // 0x03 es 11 pero estoy diciendo 00.

	//PWM1PR = 25000-1;
	PWM1PR = 0x0;

	SetPINSEL(1,18,2); //???
	SetPINMODE(1,18,2);

	PWM1MR0 = 20000;		//20000us = 20ms period duration
	PWM1MR1 = 1000;

	PWM1IR = (0x01);

	PWM1MCR |= (0x01<<1);


	PWM1LER |= (0x01<<0);
	PWM1LER |= (0x01<<1);


	PWM1PCR |= (0x01<<9);

	PWM1TCR |= (0x01<<1); //Reset Timer
	PWM1TCR |= (0x01);
	PWM1TCR |= (0x01<<3);

	PWM1CTCR &= ~(0x03); // le decimos que sea timer mode. he TC is incremented when the Prescale Counter matches the Prescale Register

	while(1)
	        {
			for (i=1000;i<2000;i++){
	          PWM1MR1 = i; // pulse width 1.5ms
	          PWM1LER |=  (0x01); //Apply changes
			}
	   }

	//25000 clock cycles @25Mhz = 1 mS

	return 0;
}

void initTimer0(void)
{
	CCLKCFG  = CCLKCFG_Value;
	PCLKSEL0  = PCLKSEL0_Value;
	CLKSRCSEL = CLKSRCSEL_Value;

	PLL0CFG   = PLL0CFG_Value;      /* configure PLL0                     */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	PLL0CON   = 0x01;             /* PLL0 Enable                        */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & (1<<26)));/* Wait for PLOCK0                    */

	PLL0CON   = 0x03;             /* PLL0 Enable & Connect              */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;


}

